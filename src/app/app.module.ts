import { SalaService } from './_service/sala.service';
import { PeliculaService } from './_service/pelicula.service';
import { OcupacionService } from './_service/ocupacion.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule,LOCALE_ID } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { MaterialModule } from './material/material.module';
import { ClienteComponent } from './pages/cliente/cliente.component';
import { ClienteService } from './_service/cliente.service';
import { HttpClientModule } from '@angular/common/http';
import { ClienteEdicionComponent } from './pages/cliente/cliente-edicion/cliente-edicion.component';
//Para trabajar con frmuarios
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { LoginService } from './_service/login.service';
import { GuardService } from './_service/guard.service';
import { EntradaService } from './_service/entrada.service';
import { CalificacionService } from './_service/calificacion.service';
import { ButacaService } from './_service/butaca.service';
import { FuncionService } from './_service/funcion.service';
import { TicketService } from './_service/ticket.service';
import { ButacaComponent } from './pages/butaca/butaca.component';
import { ButacaEdicionComponent } from './pages/butaca/butaca-edicion/butaca-edicion.component';
import { CalificacionComponent } from './pages/calificacion/calificacion.component';
import { CalificacionEdicionComponent } from './pages/calificacion/calificacion-edicion/calificacion-edicion.component';
import { EntradaComponent } from './pages/entrada/entrada.component';
import { EntradaEdicionComponent } from './pages/entrada/entrada-edicion/entrada-edicion.component';
import { FuncionComponent } from './pages/funcion/funcion.component';
import { FuncionEdicionComponent } from './pages/funcion/funcion-edicion/funcion-edicion.component';
import { OcupacionComponent } from './pages/ocupacion/ocupacion.component';
import { OcupacionEdicionComponent } from './pages/ocupacion/ocupacion-edicion/ocupacion-edicion.component';
import { PeliculaComponent } from './pages/pelicula/pelicula.component';
import { PeliculaEdicionComponent } from './pages/pelicula/pelicula-edicion/pelicula-edicion.component';
import { TicketComponent } from './pages/ticket/ticket.component';
import { TicketEdicionComponent } from './pages/ticket/ticket-edicion/ticket-edicion.component';
import { SalaComponent } from './pages/sala/sala.component';
import { SalaEdicionComponent } from './pages/sala/sala-edicion/sala-edicion.component';
import { RegistroComponent } from './resgistro/resgistro.component';
import { RegistroService } from './_service/registro.service';
import { ComprardtoService } from './_service/comprardto.service';
import { ComprardtoComponent } from './pages/comprardto/comprardto.component';
/*import { MAT_DATE_LOCALE } from '@angular/material';*/
import { UserclientedtoService } from './_service/userclientedto.service';
import { Not403Component } from './pages/not403/not403.component';
import localeEs from '@angular/common/locales/es';
import { registerLocaleData } from '@angular/common';
import { TarjetaService } from './_serviceTarjeta/tarjeta.service';
import { PagoService } from './_serviceTarjeta/pago.service';
import { PagoclientedtoService } from './_serviceTarjeta/pagoclientedto.service';
registerLocaleData(localeEs);

@NgModule({
  declarations: [
    AppComponent,
    ClienteComponent,
    ClienteEdicionComponent,
    LoginComponent,
    ButacaComponent,
    ButacaEdicionComponent,
    CalificacionComponent,
    CalificacionEdicionComponent,
    EntradaComponent,
    EntradaEdicionComponent,
    FuncionComponent,
    FuncionEdicionComponent,
    OcupacionComponent,
    OcupacionEdicionComponent,
    PeliculaComponent,
    PeliculaEdicionComponent,
    TicketComponent,
    TicketEdicionComponent,
    SalaComponent,
    SalaEdicionComponent,
    RegistroComponent,
    ComprardtoComponent,
    Not403Component,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ], 
  providers: [
    ClienteService,UserclientedtoService, ComprardtoService,RegistroService,LoginService, 
    GuardService,ButacaService,CalificacionService,EntradaService,FuncionService,OcupacionService,
    PagoclientedtoService,PeliculaService,TicketService,SalaService, TarjetaService,PagoService,
   /* { provide: MAT_DATE_LOCALE, useValue: 'es-ES  ' },*/ // Toda esta linea resuelve el idioma del calendario "picker" en comprar
   {provide: LOCALE_ID, useValue:"es" }
  ], 
  bootstrap: [AppComponent]
})
export class AppModule { }
