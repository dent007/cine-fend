import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Registro } from '../_model/registro';
import { RegistroService } from '../_service/registro.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Cliente } from '../_model/cliente';
import { Usuario } from '../_model/usuario';

@Component({
  selector: 'app-resgistro',
  templateUrl: './resgistro.component.html',
  styleUrls: ['./resgistro.component.css']
})
export class RegistroComponent implements OnInit {

  registro:Registro;
  usuario:Usuario;
  cliente:Cliente;

  form: FormGroup;

  constructor(private registroService:RegistroService, private route:ActivatedRoute, private router:Router) {
    this.registro= new Registro();
    this.cliente= new Cliente();
    this.usuario=new Usuario();

    /*this.form = new FormGroup({

      'nombre':new FormControl(''),
      'apellido':new FormControl(''),
      'documento':new FormControl(''),
      'username':new FormControl(''),
      'password':new FormControl('')
    });*/
   }

  ngOnInit() {
    console.log("this.cliente");
    this.form = new FormGroup({

      'nombre':new FormControl(''),
      'apellido':new FormControl(''),
      'documento':new FormControl(''),
      'username':new FormControl(''),
      'password':new FormControl('')
    });
  }

  private registrar(){

    this.registro.nombre=this.form.value['nombre'];
    this.registro.apellido=this.form.value['apellido'];
    this.registro.documento=this.form.value['documento'];
    this.registro.username=this.form.value['username'];
    this.registro.password=this.form.value['password'];
//console.log(this.registro)
    this.cliente.nombre=this.registro.nombre;
    this.cliente.apellido=this.registro.apellido;
    this.cliente.documento=this.registro.documento;
    this.cliente.created=new Date();
    this.cliente.update=new Date();

    this.usuario.username=this.registro.username;
    this.usuario.password=this.registro.password;
    this.usuario.authority="ROLE_USER";
    this.usuario.enabled=true;
    
    this.cliente.usuario=this.usuario;

    //console.log(this.cliente);
    this. registroService.nuevoRegistro(this.cliente).subscribe(retorno=>{
      if(retorno===1){
        this.router.navigate(['login']);

      }else{
        this.router.navigate(['registro']);

      }
    });
  }

}
/*    {
        "cliente_id": 1,
        "usuario": {
            "usuario_id": 1,
            "username": "admin",
            "password": "123",
            "authority": "ROLE_ADMIN",
            "enabled": true,
            "accountNonExpired": true,
            "accountNonLocked": true,
            "credentialsNonExpired": true,
            "authorities": [
                {
                    "authority": "ROLE_ADMIN"
                }
            ]
        },
        "nombre": "drre",
        "apellido": "Weer",
        "documento": "1234",
        "created": "2019-03-25T13:21:23.290",
        "update": "2019-03-25T13:21:23.290"
    } */