
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HOST, TOKEN_NAME, CINE_W, TARJETA_W } from '../_shared/var.constant';
import { Pagoclientedto } from '../_modelTarjeta/pagoclientedto';
import { Disponibilidadtarjetapagodto } from '../_modelTarjeta/disponibilidadtarjetapagodto';

@Injectable()
export class PagoclientedtoService {

  private url: string = `${HOST}/${TARJETA_W}/tarjeta`;

  constructor(private http: HttpClient) { }

  getEstadoPagoTarjetaCliente(disponibilidadtarjetapagodto: Disponibilidadtarjetapagodto) {
     // console.log("sssssssssssssssssssssssssssssss");
   //console.log(pagoclientedto );
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.post(`${this.url}/estado`, disponibilidadtarjetapagodto, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }


  registrarPagoTarjetaCliente(pagoclientedto: Pagoclientedto) {
    // console.log("sssssssssssssssssssssssssssssss");
  //console.log(pagoclientedto );
   let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
   return this.http.post(`${this.url}/pagando`, pagoclientedto, {
     headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
   });
 } 
}
/**
  {
	"clienteId":2,
	"tarjetaNumero":"1111111111111111",
	"tipo":"DEBITO",
	
	"valorEntrada":23, 
	"uuid":"4344"
}

**/