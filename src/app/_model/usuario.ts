export class Usuario{
    usuario_id:number;
    username:string;
    password:string;
    authority:string;
    enabled:boolean;
}