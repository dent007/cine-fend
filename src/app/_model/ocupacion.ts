import { Entrada } from "./entrada";
import { Funcion } from "./funcion";
import { Butaca } from "./butaca";
import { Ticket } from "./ticket";

export class  Ocupacion{
    public ocupacion_id:number;
    public funcion:Funcion;
    public butaca:Butaca;
    public ticket:Ticket;
    public entrada:Entrada;
    public valor:number;
    public created: Date;
    public update:Date;
}