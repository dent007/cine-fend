import { Calificacion } from "./calificacion";

export class  Pelicula{
    public pelicula_id:number;
    public calificacion:Calificacion;
    public titulo:string;
    public estreno: Date;
    public created: Date;
    public update:Date;
}
/*
    {
 {
        "pelicula_id": 1,
        "calificacion": {
            "calificacion_id": 1,
            "descripcion": "ATP",
            "created": "2019-04-15T16:32:27.451",
            "update": "2019-04-15T16:32:27.451"
        },
        "titulo": "BEBES",
        "estreno": "2019-01-05T05:30:45",
        "created": "2019-01-05T05:30:45",
        "update": "2019-01-05T05:30:45"
    }
    }
     */