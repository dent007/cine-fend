import { Sala } from "./sala";

export class  Butaca{
    public butaca_id: number;
    public sala:Sala;
    public fila:string;
    public numero:number;
    public descripcion:string;
    public created: Date;
    public update:Date;
}