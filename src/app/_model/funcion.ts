import { Pelicula } from "./pelicula";
import { Sala } from "./sala";

export class  Funcion{
    public funcion_id:number;
    public fecha: Date;
    public sala:Sala;
    public pelicula:Pelicula;
    public created: Date;
    public update:Date;
}