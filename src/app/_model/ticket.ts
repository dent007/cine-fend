import { Cliente } from "./cliente";

export class  Ticket{ 

    public ticket_id : number;
    public cliente : Cliente;
    public fecha_transaccion : Date;
    public butacas : number; // es el número de butaca/s
    public importe : number;
    public pago_uuid : string;
    public created : Date;
    public update : Date;

}