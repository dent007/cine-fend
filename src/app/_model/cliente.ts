import { Usuario } from "./usuario";

export class Cliente{
    
    cliente_id: number;
    nombre: string;
    apellido: string;
    //documento: number;
    documento: string;
    created: Date;
    update:Date;
    usuario: Usuario;
    
} 