import { Cliente } from './../../../_model/cliente';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Ticket } from '../../../_model/ticket';
import { TicketService } from '../../../_service/ticket.service';

@Component({
  selector: 'app-ticket-edicion',
  templateUrl: './ticket-edicion.component.html',
  styleUrls: ['./ticket-edicion.component.css']
})
export class TicketEdicionComponent implements OnInit {
  id:number;
  ticket:Ticket;
  form: FormGroup;
  edicion: boolean=false; //edicion o nuevo cliente
  
   constructor(private ticketService:TicketService, private route:ActivatedRoute, private router:Router) { 

    this.ticket= new Ticket();

    //cajas del html-formulario. Paso valores en blanco hacia el formulario 
    this.form = new FormGroup({
    'id':new FormControl(0),
    'cliente':new FormControl(''),
    'fecha_transaccion':new FormControl(''),
    'butacas':new FormControl(''),
    'importe':new FormControl(''),
    'pago_uuid':new FormControl(''),
        
  });
}

  ngOnInit() { //capturar id_cliente 
    this.route.params.subscribe( (parametrosUrl:Params) =>{
      this.id = parametrosUrl['id']; // Solo obtengo this.id Global
      /* si hay un id (obvio que es !=null) = > edición == V sino F*/
       this.edicion = parametrosUrl['id'] != null;// v=v=v => v , sino F 
       this.initForm();
    });
  }

  private initForm() {
    if(this.edicion) { //UPDATE
      this.ticketService.getTicketPorId(this.id).subscribe(backEndTicketId => {
        //obtengo los valores desde el Servicio quién recibio json con data de backEnd
        let id =  backEndTicketId.ticket_id;
        let cliente = backEndTicketId.cliente;//TRAIGO
        let fecha_transaccion = backEndTicketId.fecha_transaccion;//CLIENTE
        let butacas= backEndTicketId.butacas;//NUEVAMENTE
        let importe = backEndTicketId.importe;//NUEVAMENTE
        let pago_uuid = backEndTicketId.pago_uuid;//NUEVAMENTE
        //PASO backEndClienteId a formuario
        let created = backEndTicketId.created;
 
        //PASO backEndClienteId a formuario
        this.form = new FormGroup({
          'id':new FormControl(id),
          'cliente':new FormControl(cliente.cliente_id),
          'fecha_transaccion':new FormControl(fecha_transaccion),
          'butacas':new FormControl(butacas),
          'importe':new FormControl(importe),
          'pago_uuid':new FormControl(pago_uuid),
          'created':new FormControl(created)
        });
      });
    }
  }

  //capturando valoes de cajas de texto en formulario
  operar(){ // Capturo contenido de cajas/campos  de formulario y vinculado con formControlName
    this.ticket.ticket_id = this.form.value['id'];

    let cT= new Cliente();
    cT.cliente_id = this.form.value['cliente'];

    this.ticket.cliente = cT; 
    this.ticket.fecha_transaccion = this.form.value['fecha_transaccion'];
    this.ticket.butacas = this.form.value['butacas'];
    this.ticket.importe = this.form.value['importe'];
    this.ticket.pago_uuid = this.form.value['pago_uuid'];
    //this.ticket.created = this.form.value['created'];
    this.ticket.update =new Date();

    if ( /*this.edicion*/ this.ticket !=null && this.ticket.ticket_id > 0) { // viene algo, un cliente existente
      //UPDATE
      this.ticket.created = this.form.value['created'];
      this.ticketService.modificar(this.ticket).subscribe(returnBackEnd => {
       
        if(returnBackEnd === 1){ //1 = modificación exitosa
          /***Luego de modificarl llamo nuevamente a componente y muestre cambios***/
          //preparamos para mostrar nuevamente los datos en clienteComponet 
          this.ticketService.getlistarTicket().subscribe(listaTicketBackEnd => {
            console.log(listaTicketBackEnd);

            //La listaClientesBackEnd ahora es reactiva, y será llamada desde clienteComponent via clienteCambioReactivo.
            this.ticketService.ticketCambio.next(listaTicketBackEnd);
            this.ticketService.mensaje.next("Se modificó la butaca");
            //CON NEXT INDICO A clienteCambioReactivo QUE SE EXE DONDE SE INSTACIE
          });
        }else{//0 = modificación NO exitosa
          this.ticketService.mensaje.next("No se modificó la butaca");
        }
      });
    }else{
      //INSERT (inicio consumo de servicio REST)
      this.ticket.created = new Date();
      this.ticketService.registrar(this.ticket).subscribe(returnBanckEnd=>{
        console.log(returnBanckEnd);
          if(returnBanckEnd > 0){// 1 = exito
            this.ticketService.getlistarTicket().subscribe(listaButacasBackEnd=>{
              console.log(listaButacasBackEnd);

              this.ticketService.ticketCambio.next(listaButacasBackEnd);
              this.ticketService.mensaje.next("Se registró el ticket");
            }); 
            
          }else{ // 0 = error!
            this.ticketService.mensaje.next("No Se registró el ticket");
          }
      });
    }

    this.router.navigate(['ticket']);
  }
}
