import { Ticket } from '../../_model/ticket';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatTableDataSource, MatPaginator, MatSnackBar } from '@angular/material';
import { TicketService } from '../../_service/ticket.service';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.css']
})
export class TicketComponent implements OnInit {

  //VARIABLES LOCALES
  //listaClientesC: Cliente[]=[];
  displayedColumns = ['ticket_id', 'cliente', 'fecha_transaccion', 'butacas', 'importe','pago_uuid', 'created','update','acciones'];
  dataSource:MatTableDataSource<Ticket>;
  mensaje:string;
  cantidad:number;
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  /*@ViewChild(MatPaginator) paginatorr: MatPaginator;
  // @ViewChild(MatSort) sortt: MatSort;*/
   
  //INYECCONES
  constructor(private ticketService:TicketService,public route:ActivatedRoute, public snackBar:MatSnackBar) {

   }

  ngOnInit() {  //INICIALIZANDO VARIABLES LOCALES QUE SE REFLEJAN EN EL html
    
    //paciente service recibe un observable. Cuando instancien objeto tickectCambio.next()-> salto!
    this.ticketService.ticketCambio.subscribe(listaReactivaCambiada => {//subscribe contiene contenido de observable
     this.dataSource = new MatTableDataSource(listaReactivaCambiada);
     this.dataSource.paginator= this.paginator;
      this.dataSource.sort= this.sort;
    });
    //cuando instancien objeto mensaje.nex() tipo Subject, yo salto!!!
    this.ticketService.mensaje.subscribe(data => {
      this.snackBar.open(data,null,{
      duration:300,
      });
   });

    this.ticketService.getlistarTicket().subscribe(ticketObservr => {
      this.dataSource = new MatTableDataSource((ticketObservr));
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort= this.sort;  
     }); 
  }
  
  applyFilter(filterValue:string ){ // recibe texto     
    filterValue = filterValue.trim(); // limpia espacios en blanco
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue; // busca texto 
  }

  eliminar(ticket: Ticket): void {
    this.ticketService.eliminar(ticket).subscribe(data => {
      if (data === 1) {
        this.ticketService.getlistarTicket().subscribe(tick => {
          this.ticketService.ticketCambio.next(tick);
          this.ticketService.mensaje.next("El ticket se elimino correctamente");
        });
      } else {
        this.ticketService.mensaje.next("El ticket no se pudo eliminar");
      }

    });
  }
}