import { CalificacionService } from './../../../_service/calificacion.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Calificacion } from '../../../_model/calificacion';

@Component({
  selector: 'app-calificacion-edicion',
  templateUrl: './calificacion-edicion.component.html',
  styleUrls: ['./calificacion-edicion.component.css']
})
export class CalificacionEdicionComponent implements OnInit {

  id:number;
  calificacion:Calificacion;
  form: FormGroup;
  edicion: boolean=false; //edicion o nuevo cliente
  
   constructor(private calificacionService:CalificacionService, private route:ActivatedRoute, private router:Router) { 

    this.calificacion= new Calificacion();

    //cajas del html-formulario. Paso valores en blanco hacia el formulario 
    this.form = new FormGroup({
    'id':new FormControl(0),
    'descripcion':new FormControl('')       
  });
}

  ngOnInit() { //capturar id_cliente 
    this.route.params.subscribe( (parametrosUrl:Params) =>{
      this.id = parametrosUrl['id']; // Solo obtengo this.id Global
      /* si hay un id (obvio que es !=null) = > edición == V sino F*/
       this.edicion = parametrosUrl['id'] != null;// v=v=v => v , sino F 
       this.initForm();
    });
  }

  private initForm() {
    if(this.edicion) { //UPDATE
      this.calificacionService.getCalificacionPorId(this.id).subscribe(backEndCalificacionId => {
        //obtengo los valores desde el Servicio quién recibio json con data de backEnd
        let id =  backEndCalificacionId.calificacion_id;
        let descripcion = backEndCalificacionId.descripcion;//TRAIGO   
        //PASO del backEndClienteId la fecha decreación al formuario
        let created = backEndCalificacionId.created;
        //console.log(created);
        //PASO backEndClienteId a formuario
        this.form = new FormGroup({
          'id':new FormControl(id),
          'descripcion':new FormControl(descripcion),         
          'created':new FormControl(created)       

        });
      });
    }
  }

  //capturando valoes de cajas de texto en formulario
  operar(){ // Capturo contenido de cajas/campos  de formulario y vinculado con formControlName
    this.calificacion.calificacion_id = this.form.value['id'];


    this.calificacion.descripcion = this.form.value['descripcion'];
   // this.calificacion.created = this.form.value['created'];
    this.calificacion.update =new Date();

    if ( /*this.edicion*/ this.calificacion !=null && this.calificacion.calificacion_id > 0) { // viene algo, un cliente existente
      //UPDATE
      this.calificacion.created = this.form.value['created'];

     
      
      this.calificacionService.modificar(this.calificacion).subscribe(returnBackEnd => {
    
        console.log(returnBackEnd);
    
        if(returnBackEnd === 1){ //1 = modificación exitosa
          /***Luego de modificarl llamo nuevamente a componente y muestre cambios***/
          //preparamos para mostrar nuevamente los datos en clienteComponet 
          this.calificacionService.getlistarCalificacion().subscribe(listaCalificacionBackEnd => {
          //  console.log(listaCalificacionBackEnd);

            //La listaClientesBackEnd ahora es reactiva, y será llamada desde clienteComponent via clienteCambioReactivo.
            this.calificacionService.calificacionCambio.next(listaCalificacionBackEnd);
            this.calificacionService.mensaje.next("Se modificó la butaca");
            //CON NEXT INDICO A clienteCambioReactivo QUE SE EXE DONDE SE INSTACIE
          });
        }else{//0 = modificación NO exitosa
          this.calificacionService.mensaje.next("No se modificó la calificación");
        }
      });
    }else{
      //INSERT (inicio consumo de servicio REST)
      this.calificacion.created = new Date();
      this.calificacionService.registrar(this.calificacion).subscribe(returnBanckEnd=>{
        console.log(returnBanckEnd);
          if(returnBanckEnd === 1){// 1 = exito
            this.calificacionService.getlistarCalificacion().subscribe(listarCalificacionBackEnd=>{
              console.log(listarCalificacionBackEnd);

              this.calificacionService.calificacionCambio.next(listarCalificacionBackEnd);
              this.calificacionService.mensaje.next("Se registró el ticket");
            }); 
            
          }else{ // 0 = error!
            this.calificacionService.mensaje.next("No Se registró el ticket");
          }
      });
    }

    this.router.navigate(['calificacion']);
  }
}
