import { Component, OnInit, ViewChild } from '@angular/core';
import { Calificacion } from '../../_model/calificacion';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';
import { CalificacionService } from '../../_service/calificacion.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-calificacion',
  templateUrl: './calificacion.component.html',
  styleUrls: ['./calificacion.component.css']
})
export class CalificacionComponent implements OnInit {

  //VARIABLES LOCALES
  //listaClientesC: Cliente[]=[];
  displayedColumns = ['calificacion_id', 'descripcion', 'created','update','acciones'];
  dataSource:MatTableDataSource<Calificacion>;
  mensaje:string;
  //cantidad:number;
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  /*@ViewChild(MatPaginator) paginatorr: MatPaginator;
  // @ViewChild(MatSort) sortt: MatSort;*/
   
  //INYECCONES
  constructor(private calificacionService:CalificacionService,public route:ActivatedRoute, public snackBar:MatSnackBar) {

   }

  ngOnInit() {  //INICIALIZANDO VARIABLES LOCALES QUE SE REFLEJAN EN EL html
    
    //paciente service recibe un observable. Cuando instancien objeto tickectCambio.next()-> salto!
    this.calificacionService.calificacionCambio.subscribe(listaReactivaCambiada => {//subscribe contiene contenido de observable
     this.dataSource = new MatTableDataSource(listaReactivaCambiada);
     this.dataSource.paginator= this.paginator;
      this.dataSource.sort= this.sort;
    });
    //cuando instancien objeto mensaje.nex() tipo Subject, yo salto!!!
    this.calificacionService.mensaje.subscribe(data => {
      this.snackBar.open(data,null,{
      duration:300,
      });
   });

    this.calificacionService.getlistarCalificacion().subscribe(ticketObservr => {
      this.dataSource = new MatTableDataSource((ticketObservr));
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort= this.sort;  
     }); 
  }
  
  applyFilter(filterValue:string ){ // recibe texto     
    filterValue = filterValue.trim(); // limpia espacios en blanco
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue; // busca texto 
  }

  eliminar(calificacin: Calificacion): void {
    this.calificacionService.eliminar(calificacin).subscribe(data => {
      if (data === 1) {
        this.calificacionService.getlistarCalificacion().subscribe(calf => {
          this.calificacionService.calificacionCambio.next(calf);
          this.calificacionService.mensaje.next("El ticket se elimino correctamente");
        });
      } else {
        this.calificacionService.mensaje.next("El ticket no se pudo eliminar");
      }

    });
  }
}