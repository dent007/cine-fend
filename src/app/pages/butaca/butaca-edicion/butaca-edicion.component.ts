import { Component, OnInit } from '@angular/core';
import { Butaca } from '../../../_model/butaca';
import { FormGroup, FormControl } from '@angular/forms';
import { ButacaService } from '../../../_service/butaca.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Sala } from '../../../_model/sala';

@Component({
  selector: 'app-butaca-edicion',
  templateUrl: './butaca-edicion.component.html',
  styleUrls: ['./butaca-edicion.component.css']
})
export class ButacaEdicionComponent implements OnInit {



  id:number;
  butaca:Butaca;
  form: FormGroup;
  edicion: boolean=false; //edicion o nuevo cliente
  idB:number;
  
   constructor(private butacaService:ButacaService, private route:ActivatedRoute, private router:Router) { 

    this.butaca= new Butaca();

    //cajas del html-formulario. Paso valores en blanco hacia el formulario 
    this.form = new FormGroup({
    'id':new FormControl(0),
    'sala':new FormControl(''),
    'fila':new FormControl(''),
    'numero':new FormControl(''),
    'descripcion':new FormControl('')
        
  });
}

  ngOnInit() { //capturar id_cliente 
    this.route.params.subscribe( (parametrosUrl:Params) =>{
      this.id = parametrosUrl['id']; // Solo obtengo this.id Global
      /* si hay un id (obvio que es !=null) = > edición == V sino F*/
       this.edicion = parametrosUrl['id'] != null;// v=v=v => v , sino F 
       this.initForm();
    });
  }

  private initForm() {
    if(this.edicion) { //UPDATE
      this.butacaService.getButacaPorId(this.id).subscribe(backEndButacaId => {
        //obtengo los valores desde el Servicio quién recibio json con data de backEnd
        let id =  backEndButacaId.butaca_id;
        let sala = backEndButacaId.sala;//TRAIGO
        let fila = backEndButacaId.fila;//CLIENTE
        let numero= backEndButacaId.numero;//NUEVAMENTE
        let descripcion = backEndButacaId.descripcion;//NUEVAMENTE
        //PASO backEndClienteId a formuario
        let created = backEndButacaId.created;
 
        //PASO backEndClienteId a formuario
        this.form = new FormGroup({
          'id':new FormControl(id),
          'sala':new FormControl(sala.sala_id),
          'fila':new FormControl(fila),
          'numero':new FormControl(numero),
          'descripcion':new FormControl(descripcion),
          'created':new FormControl(created)
        });
      });
    }
  }

  //capturando valoes de cajas de texto en formulario
  operar(){ // Capturo contenido de cajas/campos  de formulario y vinculado con formControlName
    this.butaca.butaca_id = this.form.value['id'];
    let bu:number;
    let sP= new Sala();
    sP.sala_id = this.form.value['sala'];

    this.butaca.sala = sP; 
    this.butaca.fila = this.form.value['fila'];
    this.butaca.numero = this.form.value['numero'];
    this.butaca.descripcion = this.form.value['descripcion'];
    this.butaca.update =new Date();

    if ( /*this.edicion*/ this.butaca !=null && this.butaca.butaca_id > 0) { // viene algo, un cliente existente
      //UPDATE
      this.butaca.created = this.form.value['created'];
      this.butacaService.modificar(this.butaca).subscribe(returnBackEnd => {
       
        console.log(returnBackEnd);

        if(returnBackEnd === 1){ //1 = modificación exitosa
          /***Luego de modificarl llamo nuevamente a componente y muestre cambios***/
          //preparamos para mostrar nuevamente los datos en clienteComponet 
          this.butacaService.getlistarButaca().subscribe(listaButacasBackEnd => {
            console.log(listaButacasBackEnd);

            //La listaClientesBackEnd ahora es reactiva, y será llamada desde clienteComponent via clienteCambioReactivo.
            this.butacaService.butacaCambio.next(listaButacasBackEnd);
            this.butacaService.mensaje.next("Se modificó la butaca");
            //CON NEXT INDICO A clienteCambioReactivo QUE SE EXE DONDE SE INSTACIE
          });
        }else{//0 = modificación NO exitosa
          this.butacaService.mensaje.next("No se modificó la butaca");
        }
      });
    }else{
      //INSERT (inicio consumo de servicio REST)
      this.butaca.created = new Date();
     
      this.butacaService.registrar(this.butaca).subscribe(returnBanckEnd=>{
        console.log("mmmmmm" +returnBanckEnd);
          if(returnBanckEnd > 0){// 1 = exito
            this.butacaService.getlistarButaca().subscribe(listaButacasBackEnd=>{
              console.log(listaButacasBackEnd);

              this.butacaService.butacaCambio.next(listaButacasBackEnd);
              this.butacaService.mensaje.next("Se registró el butaca");
            }); 
            
          }else{ // 0 = error!
            this.butacaService.mensaje.next("No Se registró el butaca");
          }
      });
    }

    this.router.navigate(['butaca']);
  }
}
