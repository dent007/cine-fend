import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Butaca } from '../../_model/butaca';
import { MatSort, MatTableDataSource, MatPaginator, MatSnackBar } from '@angular/material';
import { ButacaService } from '../../_service/butaca.service';

@Component({
  selector: 'app-butaca',
  templateUrl: './butaca.component.html',
  styleUrls: ['./butaca.component.css']
})
export class ButacaComponent implements OnInit {

  //VARIABLES LOCALES
  //listaClientesC: Cliente[]=[];
  displayedColumns = ['butaca_id', 'sala', 'fila', 'numero', 'descripcion','created','update','acciones'];
  dataSource:MatTableDataSource<Butaca>;
  mensaje:string;
 // cantidad:number;
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  /*@ViewChild(MatPaginator) paginatorr: MatPaginator;
  // @ViewChild(MatSort) sortt: MatSort;*/
   
  //INYECCONES
  constructor(private butacaService:ButacaService,public route:ActivatedRoute, public snackBar:MatSnackBar) {

   }

  ngOnInit() {  //INICIALIZANDO VARIABLES LOCALES QUE SE REFLEJAN EN EL html
    
    //paciente service recibe un observable
    this.butacaService.butacaCambio.subscribe(listaReactivaCambiada => {//subscribe contiene contenido de observable
     this.dataSource = new MatTableDataSource(listaReactivaCambiada);
     this.dataSource.paginator= this.paginator;
      this.dataSource.sort= this.sort;
    });

    this.butacaService.mensaje.subscribe(data => {
      this.snackBar.open(data,null,{
      duration:300,
      });
   });

    this.butacaService.getlistarButaca().subscribe(butacaObservr => {
      this.dataSource = new MatTableDataSource((butacaObservr));
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort= this.sort;  
     }); 
  }
  
  applyFilter(filterValue:string ){ // recibe texto     
    filterValue = filterValue.trim(); // limpia espacios en blanco
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue; // busca texto 
  }

  eliminar(butaca: Butaca): void {
    this.butacaService.eliminar(butaca).subscribe(data => {
      if (data === 1) {
        this.butacaService.getlistarButaca().subscribe(butk => {
          this.butacaService.butacaCambio.next(butk);
          this.butacaService.mensaje.next("Se elimino correctamente");
        });
      } else {
        this.butacaService.mensaje.next("No se pudo eliminar");
      }

    });
  }
}