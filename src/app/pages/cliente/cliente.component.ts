import { Cliente } from './../../_model/cliente';
import { ClienteService } from './../../_service/cliente.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';


@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})
export class ClienteComponent implements OnInit {

  //VARIABLES LOCALES
  //listaClientesC: Cliente[]=[];
  displayedColumns = ['cliente_id', 'nombre', 'apellido', 'documento', 'usuario','created','update','acciones'];
  dataSource:MatTableDataSource<Cliente>;
  cantidad:number;
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  /*@ViewChild(MatPaginator) paginatorr: MatPaginator;
  // @ViewChild(MatSort) sortt: MatSort;*/
   
  //INYECCONES
  constructor(private clienteService:ClienteService, private snackBar:MatSnackBar) {

   }

  //ngOnInit solo se exe cuando se intancia a todo el componente cienteComponent.ts
  ngOnInit() {  //INICIALIZANDO VARIABLES LOCALES QUE SE REFLEJAN EN EL html
    
    //paciente service recibe un observable
    this.clienteService.getlistarClienteByPages(0, 5).subscribe(clientesObservr => {//subscribe contiene contenido de observable
      
      let clientesPagContent=JSON.parse(JSON.stringify(clientesObservr)).content;
      this.cantidad=JSON.parse(JSON.stringify(clientesObservr)).totalElements; 

      this.dataSource = new MatTableDataSource(clientesPagContent);
     // this.dataSource.paginator= this.paginator;
      this.dataSource.sort= this.sort;
    });

    // Reactivo, pintate acá cuando haya algún cambio!!!
    // ESTE MODULO REACCINARÁ SOLO ANTE CUALQUIER CAMBIO EN TABLA CLIENTES DE BD
    this.clienteService.clienteCambioReactivo.subscribe(listaReactivaCambiada => {

      let clientesPagContent=JSON.parse(JSON.stringify(listaReactivaCambiada)).content;
      this.cantidad=JSON.parse(JSON.stringify(listaReactivaCambiada)).totalElements; 

      this.dataSource= new MatTableDataSource(clientesPagContent);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort= this.sort;  
     }); 

     this.clienteService.mensajeReactivo.subscribe(mensajeAMostrar => {
      this.snackBar.open(mensajeAMostrar,null,{duration:2000} ); 
     });
  }
  eliminar(cliente: Cliente): void {
    this.clienteService.eliminar(cliente).subscribe(data => {
      if (data === 1) {
        this.clienteService.getlistarClienteByPages(0,5).subscribe(cliente => {
          this.clienteService.clienteCambioReactivo.next(cliente);
          this.clienteService.mensajeReactivo.next("Se elimino correctamente");
        });
      } else {
        this.clienteService.mensajeReactivo.next("No se pudo eliminar");
      }
    });
  }
  
  applyFilter(filterValue:string ){ // recibe texto     
    filterValue = filterValue.trim(); // limpia espacios en blanco
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue; // busca texto 
  }

  mostrarMas(e){
    this.clienteService.getlistarClienteByPages(e.pageIndex,e.pageSize).subscribe(clientesObsrvr => {
      let clientesPagConten=JSON.parse(JSON.stringify(clientesObsrvr)).content;
      this.cantidad=JSON.parse(JSON.stringify(clientesObsrvr)).totalElements; 

      this.dataSource = new MatTableDataSource(clientesPagConten);
      //this.dataSource.paginator= this.paginator;
      this.dataSource.sort= this.sort;
    });
  }

}