import { Usuario } from './../../../_model/usuario';
import { ClienteService } from './../../../_service/cliente.service';
import { Component, OnInit } from '@angular/core';
import { Cliente } from '../../../_model/cliente';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-cliente-edicion',
  templateUrl: './cliente-edicion.component.html',
  styleUrls: ['./cliente-edicion.component.css']
})
export class ClienteEdicionComponent implements OnInit {

  id:number;
  cliente:Cliente;
  form: FormGroup;
  edicion: boolean=false; //edicion o nuevo cliente
  
   constructor(private clienteService:ClienteService, private route:ActivatedRoute, private router:Router) { 

    this.cliente= new Cliente();

    //cajas del html-formulario. Paso valores en blanco hacia el formulario 
    this.form = new FormGroup({
    'id':new FormControl(0),
    'nombre':new FormControl(''),
    'apellido':new FormControl(''),
    'documento':new FormControl(''),

    'username':new FormControl(''),
    'password':new FormControl(''),
    'authority':new FormControl(''),
    'enabled':new FormControl('')
  });
}

  ngOnInit() { //Apenas cargue este componente, capturar id_cliente 
    // acceso a la url usando route (o inyecto) y de allí a sus párametros observables
    this.route.params.subscribe( (parametrosUrl:Params) =>{
      this.id = parametrosUrl['id']; // Solo obtengo this.id Global
      /* si hay un id (obvio que es !=null) = > edición == V sino F*/
       this.edicion = parametrosUrl['id'] != null;// v=v=v => v , sino F 
       this.initForm();
    });
  }

  private initForm() { // Traigo desde el BackEnd y pinto el formulario
    if(this.edicion) { //UPDATE
      this.clienteService.getClientePorId(this.id).subscribe(backEndClienteId => {
        //obtengo los valores desde el Servicio quién recibio json con data de backEnd
        let id =  backEndClienteId.cliente_id;
        let nombre = backEndClienteId.nombre;//TRAIGO
        let apellido = backEndClienteId.apellido;//CLIENTE

        let usuarioid = backEndClienteId.usuario.usuario_id;//CLIENTE
        let username = backEndClienteId.usuario.username;//CLIENTE
        let password = backEndClienteId.usuario.password;//CLIENTE
        let authority = backEndClienteId.usuario.authority;//CLIENTE
        let enabled = backEndClienteId.usuario.enabled;//CLIENTE

        let documento = backEndClienteId.documento;//NUEVAMENTE
        let created = backEndClienteId.created;

        //PASO backEndClienteId a formuario
        this.form = new FormGroup({
          'id':new FormControl(id),
          'nombre':new FormControl(nombre),
          'apellido':new FormControl(apellido),
          'documento':new FormControl(documento),

          'usuarioid':new FormControl(usuarioid),
          'username':new FormControl(username),
          'password':new FormControl(password),
          'authority':new FormControl(authority),
          'enabled':new FormControl(enabled),

          'created':new FormControl(created),
        });
      });
    }
  }

  //capturando valoes de cajas de texto en formulario
  operar(){ // Capturo contenido de cajas/campos  de formulario y vinculado con formControlName
    this.cliente.cliente_id = this.form.value['usuarioid'];
    this.cliente.nombre = this.form.value['nombre'];
    this.cliente.apellido = this.form.value['apellido'];
    this.cliente.documento = this.form.value['documento'];

    let uF = new Usuario();
    uF.usuario_id=this.form.value['id'];
    uF.username=this.form.value['username'];
    uF.password=this.form.value['password'];
    uF.authority=this.form.value['authority'];
    uF.enabled=this.form.value['enabled'];
    
    this.cliente.usuario=uF;
    this.cliente.created = this.form.value['created'];
    this.cliente.update =new Date();


    if ( this.edicion /*this.cliente !=null && this.cliente.cliente_id >0*/) { // viene algo, un cliente existente
      //UPDATE
      this.cliente.created = this.form.value['created'];
      console.log(this.cliente);
      this.clienteService.modificar(this.cliente).subscribe(returnBackEnd=>{
        if(returnBackEnd === 1){ //1 = modificación exitosa
          /***Luego de modificarl llamo nuevamente a componente y muestre cambios***/
          //preparamos para mostrar nuevamente los datos en clienteComponet 
          this.clienteService.getlistarClienteByPages(0,5).subscribe(listaClientesBackEnd => {
            //La listaClientesBackEnd ahora es reactiva, y será llamada desde clienteComponent via clienteCambioReactivo.
            this.clienteService.clienteCambioReactivo.next(listaClientesBackEnd);
            this.clienteService.mensajeReactivo.next("Se modificó el cliente");
            //CON NEXT INDICO A clienteCambioReactivo QUE SE EXE DONDE SE INSTACIE
          });
        }else{//0 = modificación NO exitosa
          this.clienteService.mensajeReactivo.next("No se modificó el cliente");
        }
      });
    }else{
      //INSERT (inicio consumo de servicio REST)
      this.cliente.created = new Date();
      this.clienteService.registrar(this.cliente).subscribe(returnBanckEnd=>{
          if(returnBanckEnd === 1){// 1 = exito
            this.clienteService.getlistarClienteByPages(0,5).subscribe(listaClientesBackEnd=>{
              this.clienteService.clienteCambioReactivo.next(listaClientesBackEnd);
              this.clienteService.mensajeReactivo.next("Se registró el cliente");
            }); 
            
          }else{ // 0 = error!
            this.clienteService.mensajeReactivo.next("No Se registró el cliente");
          }
      });
    }
    this.router.navigate(['cliente']);
  }
}
