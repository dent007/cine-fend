import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatTableDataSource, MatPaginator, MatSnackBar } from '@angular/material';
import { Pelicula } from '../../_model/pelicula';
import { PeliculaService } from '../../_service/pelicula.service';

@Component({
  selector: 'app-pelicula',
  templateUrl: './pelicula.component.html',
  styleUrls: ['./pelicula.component.css']
})
export class PeliculaComponent implements OnInit {

  //VARIABLES LOCALES
  //listaClientesC: Cliente[]=[];
  displayedColumns = ['pelicula_id', 'calificacion', 'titulo', 'estreno', 'created','update','acciones'];
  dataSource:MatTableDataSource<Pelicula>;
  mensaje:string;
  cantidad:number;
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  /*@ViewChild(MatPaginator) paginatorr: MatPaginator;
  // @ViewChild(MatSort) sortt: MatSort;*/
   
  //INYECCONES
  constructor(private peliculaService:PeliculaService,public route:ActivatedRoute, public snackBar:MatSnackBar) {

   }

  ngOnInit() {  //INICIALIZANDO VARIABLES LOCALES QUE SE REFLEJAN EN EL html
    
    //paciente service recibe un observable. Cuando instancien objeto tickectCambio.next()-> salto!
    this.peliculaService.peliculaCambio.subscribe(listaReactivaCambiada => {//subscribe contiene contenido de observable
     this.dataSource = new MatTableDataSource(listaReactivaCambiada);
     this.dataSource.paginator= this.paginator;
      this.dataSource.sort= this.sort;
    });
    //cuando instancien objeto mensaje.nex() tipo Subject, yo salto!!!
    this.peliculaService.mensaje.subscribe(data => {
      this.snackBar.open(data,null,{
      duration:300,
      });
   });

    this.peliculaService.getlistarPelicula().subscribe(peliculObservr => {
      this.dataSource = new MatTableDataSource((peliculObservr));
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort= this.sort;  
     }); 
  }
  
  applyFilter(filterValue:string ){ // recibe texto     
    filterValue = filterValue.trim(); // limpia espacios en blanco
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue; // busca texto 
  }

  eliminar(pelicula: Pelicula): void {
    this.peliculaService.eliminar(pelicula).subscribe(data => {
      if (data === 1) {
        this.peliculaService.getlistarPelicula().subscribe(tick => {
          this.peliculaService.peliculaCambio.next(tick);
          this.peliculaService.mensaje.next("La pelicula se elimino correctamente");
        });
      } else {
        this.peliculaService.mensaje.next("La peíicula no se pudo eliminar");
      }

    });
  }
}