import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Pelicula } from '../../../_model/pelicula';
import { PeliculaService } from '../../../_service/pelicula.service';
import { Calificacion } from '../../../_model/calificacion';

@Component({
  selector: 'app-pelicula-edicion',
  templateUrl: './pelicula-edicion.component.html',
  styleUrls: ['./pelicula-edicion.component.css']
})
export class PeliculaEdicionComponent implements OnInit {
  id:number;
  pelicula:Pelicula;
  form: FormGroup;
  edicion: boolean=false; //edicion o nuevo cliente
  
   constructor(private peliculaService:PeliculaService, private route:ActivatedRoute, private router:Router) { 

    this.pelicula= new Pelicula();

    //cajas del html-formulario. Paso valores en blanco hacia el formulario 
    this.form = new FormGroup({
    'id':new FormControl(0),
    'calificacion':new FormControl(''),
    'titulo':new FormControl(''),
    'estreno':new FormControl(''),
        
  });
}

  ngOnInit() { //capturar id_cliente 
    this.route.params.subscribe( (parametrosUrl:Params) =>{
      this.id = parametrosUrl['id']; // Solo obtengo this.id Global
      /* si hay un id (obvio que es !=null) = > edición == V sino F*/
       this.edicion = parametrosUrl['id'] != null;// v=v=v => v , sino F 
       this.initForm();
    });
  }

  private initForm() {
    if(this.edicion) { //UPDATE
      this.peliculaService.getPeliculaPorId(this.id).subscribe(backEndPeliculaId => {
        //obtengo los valores desde el Servicio quién recibio json con data de backEnd
        let id =  backEndPeliculaId.pelicula_id;
        let calificacion= backEndPeliculaId.calificacion;//TRAIGO
        let titulo = backEndPeliculaId.titulo;//CLIENTE
        let estreno= backEndPeliculaId.estreno;//NUEVAMENTE
        //PASO backEndClienteId a formuario
        let created = backEndPeliculaId.created;
 
        //PASO backEndClienteId a formuario
        this.form = new FormGroup({
          'id':new FormControl(id),
          'calificacion':new FormControl(calificacion.calificacion_id),
          'titulo':new FormControl(titulo),
          'estreno':new FormControl(estreno),
          'created':new FormControl(created)
        });
      });
    }
  }

  //capturando valoes de cajas de texto en formulario
  operar(){ // Capturo contenido de cajas/campos  de formulario y vinculado con formControlName
    this.pelicula.pelicula_id = this.form.value['id'];

    let cF= new Calificacion();
    cF.calificacion_id = this.form.value['calificacion'];

    this.pelicula.calificacion = cF; 
    //this.pelicula.calificacion = this.form.value['calificacion'];
    this.pelicula.titulo = this.form.value['titulo'];
    this.pelicula.estreno = this.form.value['estreno'];
    this.pelicula.update =new Date();

    if ( /*this.edicion*/ this.pelicula !=null && this.pelicula.pelicula_id > 0) { // viene algo, un cliente existente
      //UPDATE
      this.pelicula.created = this.form.value['created'];
//      console.log(pelicula)
      this.peliculaService.modificar(this.pelicula).subscribe(returnBackEnd => {
       
        if(returnBackEnd === 1){ //1 = modificación exitosa
          /***Luego de modificarl llamo nuevamente a componente y muestre cambios***/
          //preparamos para mostrar nuevamente los datos en clienteComponet 
          this.peliculaService.getlistarPelicula().subscribe(listaPeliculatBackEnd => {
            console.log(listaPeliculatBackEnd);

            //La listaClientesBackEnd ahora es reactiva, y será llamada desde clienteComponent via clienteCambioReactivo.
            this.peliculaService.peliculaCambio.next(listaPeliculatBackEnd);
            this.peliculaService.mensaje.next("Se modificó la butaca");
            //CON NEXT INDICO A clienteCambioReactivo QUE SE EXE DONDE SE INSTACIE
          });
        }else{//0 = modificación NO exitosa
          this.peliculaService.mensaje.next("No se modificó la butaca");
        }
      });
    }else{
      //INSERT (inicio consumo de servicio REST)
      this.pelicula.created = new Date();
      this.peliculaService.registrar(this.pelicula).subscribe(returnBanckEnd=>{
        console.log(returnBanckEnd);
          if(returnBanckEnd === 1){// 1 = exito
            this.peliculaService.getlistarPelicula().subscribe(listPeliculaBackEnd=>{
              console.log(listPeliculaBackEnd);

              this.peliculaService.peliculaCambio.next(listPeliculaBackEnd);
              this.peliculaService.mensaje.next("Se registró el ticket");
            }); 
            
          }else{ // 0 = error!
            this.peliculaService.mensaje.next("No Se registró el ticket");
          }
      });
    }

    this.router.navigate(['pelicula']);
  }
}
