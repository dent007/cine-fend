import { PeliculaComponent } from './../../pelicula/pelicula.component';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Funcion } from '../../../_model/funcion';
import { FuncionService } from '../../../_service/funcion.service';
import { Sala } from '../../../_model/sala';
import { Pelicula } from '../../../_model/pelicula';

@Component({
  selector: 'app-funcion-edicion',
  templateUrl: './funcion-edicion.component.html',
  styleUrls: ['./funcion-edicion.component.css']
})
export class FuncionEdicionComponent implements OnInit {
  id:number;
  funcion:Funcion;
  form: FormGroup;
  edicion: boolean=false; //edicion o nuevo cliente
  
   constructor(private funcionService:FuncionService, private route:ActivatedRoute, private router:Router) { 

    this.funcion= new Funcion();

    //cajas del html-formulario. Paso valores en blanco hacia el formulario 
    this.form = new FormGroup({
    'id':new FormControl(0),
    'sala':new FormControl(''),
    'fecha':new FormControl(''),
    'pelicula':new FormControl(''),
        
  });
}

  ngOnInit() { //capturar id_cliente 
    this.route.params.subscribe( (parametrosUrl:Params) =>{
      this.id = parametrosUrl['id']; // Solo obtengo this.id Global
      /* si hay un id (obvio que es !=null) = > edición == V sino F*/
       this.edicion = parametrosUrl['id'] != null;// v=v=v => v , sino F 
       this.initForm();
    });
  }

  private initForm() {
    if(this.edicion) { //UPDATE
      this.funcionService.getFuncionPorId(this.id).subscribe(backEndFuncionId => {
        //obtengo los valores desde el Servicio quién recibio json con data de backEnd
        let id =  backEndFuncionId.funcion_id;
        let fecha = backEndFuncionId.fecha;
        let sala= backEndFuncionId.sala;//TRAIGO
        let pelicula = backEndFuncionId.pelicula;
        //PASO backEndClienteId a formuario
        let created = backEndFuncionId.created;
 
        //PASO backEndClienteId a formuario
        this.form = new FormGroup({
          'id':new FormControl(id),
          'fecha':new FormControl(fecha),
          'sala':new FormControl(sala.sala_id),
          'pelicula':new FormControl(pelicula.pelicula_id),
          'created':new FormControl(created)
        });
      });
    }
  }

  //capturando valoes de cajas de texto en formulario
  operar(){ // Capturo contenido de cajas/campos  de formulario y vinculado con formControlName
    this.funcion.funcion_id = this.form.value['id'];

    let sF= new Sala();
    sF.sala_id = this.form.value['sala'];
    this.funcion.sala = sF;

    let  pF = new Pelicula();
    pF.pelicula_id=this.form.value['pelicula'];
    this.funcion.pelicula=pF;

    this.funcion.fecha=this.form.value['fecha'];
    this.funcion.update =new Date();

    if ( /*this.edicion*/ this.funcion !=null && this.funcion.funcion_id > 0) { // viene algo, un cliente existente
      //UPDATE
      this.funcion.created = this.form.value['created'];
      console.log(this.funcion)
      this.funcionService.modificar(this.funcion).subscribe(returnBackEnd => {
       
        if(returnBackEnd === 1){ //1 = modificación exitosa
          /***Luego de modificarl llamo nuevamente a componente y muestre cambios***/
          //preparamos para mostrar nuevamente los datos en clienteComponet 
          this.funcionService.getlistarFuncion().subscribe(listaFuncionBackEnd => {
            console.log(listaFuncionBackEnd);

            //La listaClientesBackEnd ahora es reactiva, y será llamada desde clienteComponent via clienteCambioReactivo.
            this.funcionService.funcionCambio.next(listaFuncionBackEnd);
            this.funcionService.mensaje.next("Se modificó la funcion");
            //CON NEXT INDICO A clienteCambioReactivo QUE SE EXE DONDE SE INSTACIE
          });
        }else{//0 = modificación NO exitosa
          this.funcionService.mensaje.next("No se modificó la funcion");
        }
      });
    }else{
      //INSERT (inicio consumo de servicio REST)
      this.funcion.created = new Date();
      this.funcionService.registrar(this.funcion).subscribe(returnBanckEnd=>{
          if(returnBanckEnd === 1){// 1 = exito
            this.funcionService.getlistarFuncion().subscribe(listFuncionBackEnd=>{

              this.funcionService.funcionCambio.next(listFuncionBackEnd);
              this.funcionService.mensaje.next("Se registró el funcion");
            }); 
            
          }else{ // 0 = error!
            this.funcionService.mensaje.next("No Se registró el funcion");
          }
      });
    }

    this.router.navigate(['funcion']);
  }
}
