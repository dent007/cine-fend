import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatTableDataSource, MatPaginator, MatSnackBar } from '@angular/material';
import { Funcion } from '../../_model/funcion';
import { FuncionService } from '../../_service/funcion.service';

@Component({
  selector: 'app-funcion',
  templateUrl: './funcion.component.html',
  styleUrls: ['./funcion.component.css']
})
export class FuncionComponent implements OnInit {

  displayedColumns = ['funcion_id', 'sala', 'fecha', 'pelicula', 'created','update','acciones'];
  dataSource:MatTableDataSource<Funcion>;
  mensaje:string;
  cantidad:number;
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  /*@ViewChild(MatPaginator) paginatorr: MatPaginator;
  // @ViewChild(MatSort) sortt: MatSort;*/
   
  //INYECCONES
  constructor(private funcionService:FuncionService,public route:ActivatedRoute, public snackBar:MatSnackBar) {

   }

  ngOnInit() {  //INICIALIZANDO VARIABLES LOCALES QUE SE REFLEJAN EN EL html
    
    //paciente service recibe un observable. Cuando instancien objeto tickectCambio.next()-> salto!
    this.funcionService.funcionCambio.subscribe(listaReactivaCambiada => {//subscribe contiene contenido de observable
     this.dataSource = new MatTableDataSource(listaReactivaCambiada);
     this.dataSource.paginator= this.paginator;
      this.dataSource.sort= this.sort;
    });
    //cuando instancien objeto mensaje.nex() tipo Subject, yo salto!!!
    this.funcionService.mensaje.subscribe(data => {
      this.snackBar.open(data,null,{
      duration:300,
      });
   });

    this.funcionService.getlistarFuncion().subscribe(peliculObservr => {
      this.dataSource = new MatTableDataSource((peliculObservr));
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort= this.sort;  
     }); 
  }
  
  applyFilter(filterValue:string ){ // recibe texto     
    filterValue = filterValue.trim(); // limpia espacios en blanco
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue; // busca texto 
  }

  eliminar(funcion: Funcion): void {
    this.funcionService.eliminar(funcion).subscribe(data => {
      if (data === 1) {
        this.funcionService.getlistarFuncion().subscribe(tick => {
          this.funcionService.funcionCambio.next(tick);
          this.funcionService.mensaje.next("La pelicula se elimino correctamente");
        });
      } else {
        this.funcionService.mensaje.next("La peíicula no se pudo eliminar");
      }

    });
  }
}