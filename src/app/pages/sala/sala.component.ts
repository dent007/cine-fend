import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatTableDataSource, MatPaginator, MatSnackBar } from '@angular/material';
import { Sala } from '../../_model/sala';
import { SalaService } from '../../_service/sala.service';

@Component({
  selector: 'app-sala',
  templateUrl: './sala.component.html',
  styleUrls: ['./sala.component.css']
})
export class SalaComponent implements OnInit {

  displayedColumns = ['sala_id', 'descripcion', 'capacidad', 'created','update','acciones'];
  dataSource:MatTableDataSource<Sala>;
  mensaje:string;
 // cantidad:number;
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  /*@ViewChild(MatPaginator) paginatorr: MatPaginator;
  // @ViewChild(MatSort) sortt: MatSort;*/
   
  //INYECCONES
  constructor(private salaService:SalaService,public route:ActivatedRoute, public snackBar:MatSnackBar) {

   }

  ngOnInit() {  //INICIALIZANDO VARIABLES LOCALES QUE SE REFLEJAN EN EL html
    
    //paciente service recibe un observable
    this.salaService.salaCambio.subscribe(listaReactivaCambiada => {//subscribe contiene contenido de observable
     this.dataSource = new MatTableDataSource(listaReactivaCambiada);
     this.dataSource.paginator= this.paginator;
      this.dataSource.sort= this.sort;
    });

    this.salaService.mensaje.subscribe(data => {
      this.snackBar.open(data,null,{
      duration:300,
      });
   });

    this.salaService.getlistarSala().subscribe(salaObservr => {
      this.dataSource = new MatTableDataSource((salaObservr));
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort= this.sort;  
     }); 
  }
  
  applyFilter(filterValue:string ){ // recibe texto     
    filterValue = filterValue.trim(); // limpia espacios en blanco
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue; // busca texto 
  }

  eliminar(sala: Sala): void {
    this.salaService.eliminar(sala).subscribe(data => {
      if (data === 1) {
        this.salaService.getlistarSala().subscribe(sal => {
          this.salaService.salaCambio.next(sal);
          this.salaService.mensaje.next("Se elimino correctamente");
        });
      } else {
        this.salaService.mensaje.next("No se pudo eliminar");
      }

    });
  }
}