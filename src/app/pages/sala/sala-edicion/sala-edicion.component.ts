import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Sala } from '../../../_model/sala';
import { SalaService } from '../../../_service/sala.service';

@Component({
  selector: 'app-sala-edicion',
  templateUrl: './sala-edicion.component.html',
  styleUrls: ['./sala-edicion.component.css']
})
export class SalaEdicionComponent implements OnInit {
  id:number;
  sala:Sala;
  form: FormGroup;
  edicion: boolean=false; //edicion o nuevo cliente
  
   constructor(private salaService:SalaService, private route:ActivatedRoute, private router:Router) { 

    this.sala= new Sala();

    //cajas del html-formulario. Paso valores en blanco hacia el formulario 
    this.form = new FormGroup({
    'id':new FormControl(0),
    'descripcion':new FormControl(''),
    'capacidad':new FormControl('')
  });
}

  ngOnInit() { //capturar id_cliente 
    this.route.params.subscribe( (parametrosUrl:Params) =>{
      this.id = parametrosUrl['id']; // Solo obtengo this.id Global
      /* si hay un id (obvio que es !=null) = > edición == V sino F*/
       this.edicion = parametrosUrl['id'] != null;// v=v=v => v , sino F 
       this.initForm();
    });
  }

  private initForm() {
    if(this.edicion) { //UPDATE
      this.salaService.getSalaPorId(this.id).subscribe(backEndSalaId => {
        //obtengo los valores desde el Servicio quién recibio json con data de backEnd
        let id =  backEndSalaId.sala_id;
        let descripcion = backEndSalaId.descripcion;//TRAIGO
        let capacidad = backEndSalaId.capacidad;//CLIENTE
        //PASO backEndClienteId a formuariontrada_id
        let created = backEndSalaId.created;
 
        //PASO backEndClienteId a formuario
        this.form = new FormGroup({
          'id':new FormControl(id),
          'descripcion':new FormControl(descripcion),
          'capacidad':new FormControl(capacidad),
          'created':new FormControl(created)
        });
      });
    }
  }

  //capturando valoes de cajas de texto en formulario
  operar(){ // Capturo contenido de cajas/campos  de formulario y vinculado con formControlName
    this.sala.sala_id = this.form.value['id'];
    this.sala.descripcion = this.form.value['descripcion'];
    this.sala.capacidad = this.form.value['capacidad'];
    this.sala.update =new Date();

    if ( /*this.edicion*/ this.sala !=null && this.sala.sala_id > 0) { // viene algo, un cliente existente
      //UPDATE
      this.sala.created = this.form.value['created'];
      this.salaService.modificar(this.sala).subscribe(returnBackEnd => {
       
        console.log(returnBackEnd);

        if(returnBackEnd === 1){ //1 = modificación exitosa
          /***Luego de modificarl llamo nuevamente a componente y muestre cambios***/
          //preparamos para mostrar nuevamente los datos en clienteComponet 
          this.salaService.getlistarSala().subscribe(listaSalasBackEnd => {
            console.log(listaSalasBackEnd);

            //La listaClientesBackEnd ahora es reactiva, y será llamada desde clienteComponent via clienteCambioReactivo.
            this.salaService.salaCambio.next(listaSalasBackEnd);
            this.salaService.mensaje.next("Se modificó la sala");
            //CON NEXT INDICO A clienteCambioReactivo QUE SE EXE DONDE SE INSTACIE
          });
        }else{//0 = modificación NO exitosa
          this.salaService.mensaje.next("No se modificó la sala");
        }
      });
    }else{
      //INSERT (inicio consumo de servicio REST)
      this.sala.created = new Date();
      this.salaService.registrar(this.sala).subscribe(returnBanckEnd=>{
        
          if(returnBanckEnd === 1){// 1 = exito
            this.salaService.getlistarSala().subscribe(listaSalasBackEnd=>{
              console.log(listaSalasBackEnd);

              this.salaService.salaCambio.next(listaSalasBackEnd);
              this.salaService.mensaje.next("Se registró el sala");
            }); 
            
          }else{ // 0 = error!
            this.salaService.mensaje.next("No Se registró el sala");
          }
      });
    }

    this.router.navigate(['sala']);
  }
}
