import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatTableDataSource, MatPaginator, MatSnackBar } from '@angular/material';
import { Ocupacion } from '../../_model/ocupacion';
import { OcupacionService } from '../../_service/ocupacion.service';

@Component({
  selector: 'app-ocupacion',
  templateUrl: './ocupacion.component.html',
  styleUrls: ['./ocupacion.component.css']
})
export class OcupacionComponent implements OnInit {
//VARIABLES LOCALES
  //listaClientesC: Cliente[]=[];
  displayedColumns = ['ocupacion_id', 'funcion', 'butaca', 'ticket', 'entrada','valor', 'created','update','acciones'];
  dataSource:MatTableDataSource<Ocupacion>;
  mensaje:string;
  cantidad:number;
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  /*@ViewChild(MatPaginator) paginatorr: MatPaginator;
  // @ViewChild(MatSort) sortt: MatSort;*/
   
  //INYECCONES
  constructor(private ocupacionService:OcupacionService,public route:ActivatedRoute, public snackBar:MatSnackBar) {

   }

  ngOnInit() {  //INICIALIZANDO VARIABLES LOCALES QUE SE REFLEJAN EN EL html
    
    //paciente service recibe un observable. Cuando instancien objeto tickectCambio.next()-> salto!
    this.ocupacionService.ocupacionCambio.subscribe(listaReactivaCambiada => {//subscribe contiene contenido de observable
     this.dataSource = new MatTableDataSource(listaReactivaCambiada);
     this.dataSource.paginator= this.paginator;
      this.dataSource.sort= this.sort;
    });
    //cuando instancien objeto mensaje.nex() tipo Subject, yo salto!!!
    this.ocupacionService.mensaje.subscribe(data => {
      this.snackBar.open(data,null,{
      duration:300,
      });
   });

    this.ocupacionService.getlistarOcupacion().subscribe(ocupacionObservr => {
      this.dataSource = new MatTableDataSource((ocupacionObservr));
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort= this.sort;  
     }); 
  }
  
  applyFilter(filterValue:string ){ // recibe texto     
    filterValue = filterValue.trim(); // limpia espacios en blanco
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue; // busca texto 
  }

  eliminar(ocupacion: Ocupacion): void {
    this.ocupacionService.eliminar(ocupacion).subscribe(data => {
      if (data === 1) {
        this.ocupacionService.getlistarOcupacion().subscribe(ocup => {
          this.ocupacionService.ocupacionCambio.next(ocup);
          this.ocupacionService.mensaje.next("El ticket se elimino correctamente");
        });
      } else {
        this.ocupacionService.mensaje.next("El ticket no se pudo eliminar");
      }

    });
  }
}