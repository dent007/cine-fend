import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Ocupacion } from '../../../_model/ocupacion';
import { OcupacionService } from '../../../_service/ocupacion.service';
import { Funcion } from '../../../_model/funcion';
import { Butaca } from '../../../_model/butaca';
import { Ticket } from '../../../_model/ticket';
import { Entrada } from '../../../_model/entrada';

@Component({
  selector: 'app-ocupacion-edicion',
  templateUrl: './ocupacion-edicion.component.html',
  styleUrls: ['./ocupacion-edicion.component.css']
})
export class OcupacionEdicionComponent implements OnInit {
  id:number;
  ocupacion:Ocupacion;
  form: FormGroup;
  edicion: boolean=false; //edicion o nuevo cliente
  
   constructor(private ocupacionService:OcupacionService, private route:ActivatedRoute, private router:Router) { 

    this.ocupacion= new Ocupacion();

    //cajas del html-formulario. Paso valores en blanco hacia el formulario 
    this.form = new FormGroup({
    'id':new FormControl(0),
    'funcion':new FormControl(''),
    'butaca':new FormControl(''),
    'ticket':new FormControl(''),
    'entrada':new FormControl(''),
    'valor':new FormControl(''),
        
  });
}

  ngOnInit() { //capturar id_cliente 
    this.route.params.subscribe( (parametrosUrl:Params) =>{
      this.id = parametrosUrl['id']; // Solo obtengo this.id Global
      /* si hay un id (obvio que es !=null) = > edición == V sino F*/
       this.edicion = parametrosUrl['id'] != null;// v=v=v => v , sino F 
       this.initForm();
    });
  }

  private initForm() {
    if(this.edicion) { //UPDATE
      this.ocupacionService.getOcupacionPorId(this.id).subscribe(backEndOcupacionId => {
        //obtengo los valores desde el Servicio quién recibio json con data de backEnd
        let id =  backEndOcupacionId.ocupacion_id;
        let funcion = backEndOcupacionId.funcion;//TRAIGO
        let butaca = backEndOcupacionId.butaca;//CLIENTE
        let ticket= backEndOcupacionId.ticket;//NUEVAMENTE
        let entrada = backEndOcupacionId.entrada;//NUEVAMENTE
        let valor = backEndOcupacionId.valor;//NUEVAMENTE
        //PASO backEndClienteId a formuario
        let created = backEndOcupacionId.created;
 
        //PASO backEndClienteId a formuario
        this.form = new FormGroup({
          'id':new FormControl(id),
          'funcion':new FormControl(funcion.funcion_id),
          'butaca':new FormControl(butaca.butaca_id),
          'ticket':new FormControl(ticket.ticket_id),
          'entrada':new FormControl(entrada.entrada_id),
          'valor':new FormControl(valor),
          'created':new FormControl(created)
        });
      });
    }
  }

  //capturando valoes de cajas de texto en formulario
  operar(){ // Capturo contenido de cajas/campos  de formulario y vinculado con formControlName
    this.ocupacion.ocupacion_id = this.form.value['id'];

    let fF= new Funcion();
    fF.funcion_id = this.form.value['funcion'];
    this.ocupacion.funcion = fF; 

    let bF= new Butaca();
    bF.butaca_id=this.form.value['butaca'];
    this.ocupacion.butaca=bF;
    
    let tF=new Ticket();
    tF.ticket_id=this.form.value['ticket'];
    this.ocupacion.ticket = tF;

    let eF=new Entrada();
    eF.entrada_id=this.form.value['entrada'];
    this.ocupacion.entrada = eF;


    this.ocupacion.valor = this.form.value['valor'];
    //this.ticket.created = this.form.value['created'];
    this.ocupacion.update =new Date();

    if ( /*this.edicion*/ this.ocupacion !=null && this.ocupacion.ocupacion_id > 0) { // viene algo, un cliente existente
      //UPDATE
      this.ocupacion.created = this.form.value['created'];
      this.ocupacionService.modificar(this.ocupacion).subscribe(returnBackEnd => {
       
        if(returnBackEnd === 1){ //1 = modificación exitosa
          /***Luego de modificarl llamo nuevamente a componente y muestre cambios***/
          //preparamos para mostrar nuevamente los datos en clienteComponet 
          this.ocupacionService.getlistarOcupacion().subscribe(listaOcupacionBackEnd => {

            //La listaClientesBackEnd ahora es reactiva, y será llamada desde clienteComponent via clienteCambioReactivo.
            this.ocupacionService.ocupacionCambio.next(listaOcupacionBackEnd);
            this.ocupacionService.mensaje.next("Se modificó la ocupacion");
            //CON NEXT INDICO A clienteCambioReactivo QUE SE EXE DONDE SE INSTACIE
          });
        }else{//0 = modificación NO exitosa
          this.ocupacionService.mensaje.next("No se modificó la butaca");
        }
      });
    }else{
      //INSERT (inicio consumo de servicio REST)
      this.ocupacion.created = new Date();
      this.ocupacionService.registrar(this.ocupacion).subscribe(returnBanckEnd=>{
        console.log(returnBanckEnd);
          if(returnBanckEnd === 1){// 1 = exito
            this.ocupacionService.getlistarOcupacion().subscribe(listaOcupacionBackEnd=>{

              this.ocupacionService.ocupacionCambio.next(listaOcupacionBackEnd);
              this.ocupacionService.mensaje.next("Se registró el ocupacion");
            }); 
            
          }else{ // 0 = error!
            this.ocupacionService.mensaje.next("No Se registró el ocupacion");
          }
      });
    }

    this.router.navigate(['ocupacion']);
  }
}
