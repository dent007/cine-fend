import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-not403',
  templateUrl: './not403.component.html',
  styleUrls: ['./not403.component.css']
})
export class Not403Component implements OnInit {
  //name = 'Error 403';
  constructor() { }

    ngOnInit() {  
      let x:any;
      let y:any;
      let cy:any;
      let cx:any;

      var root = document.documentElement;
      var eyef = document.getElementById('eyef');
       cx = document.getElementById("eyef").getAttribute("cx");
       cy = document.getElementById("eyef").getAttribute("cy");
      
      document.addEventListener("mousemove", evt => {
        
         x = evt.clientX / innerWidth;
        y = evt.clientY / innerHeight;
      
        root.style.setProperty("--mouse-x", x);
        root.style.setProperty("--mouse-y", y);
        
        cx = 115 + 30 * x;
        cy = 50 + 30 * y;
        eyef.setAttribute("cx", cx);
        eyef.setAttribute("cy", cy);
        
      });
      
      document.addEventListener("touchmove", touchHandler => {
         x = touchHandler.touches[0].clientX / innerWidth;
         y = touchHandler.touches[0].clientY / innerHeight;
      
        root.style.setProperty("--mouse-x", x);
        root.style.setProperty("--mouse-y", y);
      }); 
    }
}

