import { Tarjeta } from './../../_modelTarjeta/tarjeta';
import { OcupacionService } from './../../_service/ocupacion.service';
import { map } from 'rxjs/operators/map';

import { TicketService } from './../../_service/ticket.service';
import { element } from 'protractor';
import { FormControl } from '@angular/forms';
import { ButacaService } from './../../_service/butaca.service';
import { FuncionService } from './../../_service/funcion.service';
import { Component, OnInit, Input } from '@angular/core';
import { Funcion } from '../../_model/funcion';
import { Butaca } from '../../_model/butaca';
import { Sala } from '../../_model/sala';
import { Calificacion } from '../../_model/calificacion';
import { Pelicula } from '../../_model/pelicula';
import { SalaService } from '../../_service/sala.service';
import { MatSnackBar } from '@angular/material';
import { PeliculaService } from '../../_service/pelicula.service';
import { Router } from '@angular/router';
import { TOKEN_NAME } from '../../_shared/var.constant';
import * as decode from 'jwt-decode';
import { ClienteService } from '../../_service/cliente.service';
import { UserclientedtoService } from '../../_service/userclientedto.service';
import { Userclientedto } from '../../_model/userclientedto';
import { Ticket } from '../../_model/ticket';
import { Ocupacion } from '../../_model/ocupacion';
import { Entrada } from '../../_model/entrada';
import { EntradaService } from '../../_service/entrada.service';
import { Usuario } from '../../_model/usuario';
import { UUID } from 'angular2-uuid';//npm install angular2-uuid --save
import { Cliente } from '../../_model/cliente';
import { PagoclientedtoService } from '../../_serviceTarjeta/pagoclientedto.service';
import { Tarjetasaldodto } from '../../_modelTarjeta/tarjetaSaldodto';
import { Disponibilidadtarjetapagodto } from '../../_modelTarjeta/disponibilidadtarjetapagodto';
import { Pagoclientedto } from '../../_modelTarjeta/pagoclientedto';

@Component({
  selector: 'app-comprardto',
  templateUrl: './comprardto.component.html',
  styleUrls: ['./comprardto.component.css']
})
export class ComprardtoComponent implements OnInit {
  
  //Listamos
  funciones: Funcion[]=[];/*lo que recorre el *ngFor del mat-option */
  idFuncionSeleccinada: number;/*lo que recibe mat-select. Sabiendo el id del elemento seleccionado, envio ese objeto a la BD */
  
  salas: Sala[]=[];
  idSalaSeleccinada: number;

  laCalif:Calificacion;
  idPeliculaCalif:number;
  butacas: Butaca[]=[];
  butacasNew: Butaca[]=[];
  idButacaSeleccinada: number;
 // filaBuata:string[]=["A","B","C","D","E","F","G","H","I","J"];
  //numeroButaca:number[]=[0,1,2,3,4,5,6,7,8,9];
  filaBuata:string[]=[];
  numeroButaca:number[]=[];
  tarjetaTipo:string[]=[];
  //Escrimos en caja input matInput para Sala
  descripcionSala:string;
  capacidadSala:number; 
  //Almacenan salas o funciones en de acordion u option
  descripcionsCapacidadsSeleccionadas: Sala[]=[];
  funcionesSeleccionadas: Funcion[]=[];

  compraControl:FormControl;

  fechaSeleccionada: Date = new Date();//inserta la fecha de hoy
  maxFecha: Date=new Date();
  
  mensaje:string;
  //@Input() funcionPeliRespons:Pelicula ;
  laFuncin:Funcion;
  nuevaButaca:Butaca;
  idButacaNumero:number;

  butacaNumero:number; 
  butacaFila:string;
  
  funcionSala_IdButaca:number;
  msgPorButaca:string;
  butaca:Butaca;
  
  laSala:Sala;
  laPelicula:Pelicula;
  ocupacionNueva:Ocupacion;
  entradaNuevas:Entrada[]=[];
  ticketNew:Ticket;
  user:Usuario;
  precioEntrada:number;
  entradaTemp:Entrada;
  uuid :any;
  display = true;
  clienteUserOn:Cliente;
  tarjetaNumero:string;
  tipoTarjeta:string;
  tarjetaReturn:string;
  flagPago:boolean;
  pagoclientedto:Pagoclientedto;
  disponibilidadtarjetapagodto:Disponibilidadtarjetapagodto;
  tarjetaSaldodto:Tarjetasaldodto;
  idTarjetaPago:number;
  constructor(
    private router: Router, private  funcionService:FuncionService, private butacaService:ButacaService,
    private salaService:SalaService,public peliculaService: PeliculaService,public clienteService:ClienteService,
    public userclientedtoService:UserclientedtoService, public ticketService:TicketService ,public snackBar: MatSnackBar,
    public ocupacionService:OcupacionService,public entradaService:EntradaService,public pagoclientedtoService:PagoclientedtoService){
    // this.laFuncin=new Funcion();
   // this.clienteUserBuscad = new Cliente();
    this.filaBuata=["A","B","C","D","E","F","G","H","I","J"];
    this.numeroButaca=[0,1,2,3,4,5,6,7,8,9];
    this.tarjetaTipo=["CREDITO","DEBITO"];
    this.ocupacionNueva=new Ocupacion();
    this.ticketNew = new Ticket();
    this.clienteUserOn= new Cliente();
   this.tarjetaSaldodto= new Tarjetasaldodto();
   }

  ngOnInit() {
    /*Para listar as listas de arriba */
    this.listarFunciones();
    this.listarButacas();
    this.listarSalas();
    this.listarEntradas();
   this.getClienteUsurio();  

  }

  onKey(event:any){
    if(event.target.value.length > 15){
      this.display =false;
    }

    this.tarjetaNumero= event.target.value;

}
 




  getEsteNumeroButaca(){
    var saltarforEach = true;
    /*console.log("salaId -> " + this.funcionSala_IdButaca);console.log("fila" + this.butacaFila);console.log("fila" + this.t);console.log("numero" + this.butacaNumero);*/
   // console.log("ID_Sala -> " + this.funcionSala_IdButaca);

   /****LISTAR TODAS LAS BUTACAS EXISTENTES EN LA BD**TRAIGO TODO*/
        this.butacas.forEach(butaks => { //Listar SOLO todas las butacas que ya se registraron y encontrar esta "funcionSala_IdButaca"
     if(saltarforEach) {
        // console.log(butaks);
      //console.log("butaks.fila -> "+ butaks.fila); console.log("butaks.numero  -> "+ butaks.numero ); console.log("butaks.butaca_id -> "+ butaks.butaca_id);console.log("butaks.sala.sala_id -> "+ butaks.sala.sala_id);
                /* console.log("Ekement->SalaPaButaca -> " + butaks.sala.sala_id);console.log("Ekement->Butaca -> " + butaks.butaca_id); console.log("ID_Sala -> " + this.funcionSala_IdButaca);*/
                /*console.log("fila llena-> " + butaks.fila); console.log("numero lleno-> " + butaks.numero); console.log("fila nueva-> " + this.butacaFila);  console.log("numero nuevo-> " + this.butacaNumero); */
                //CONSULTO BUTACA ¿¿¿LLENA o VACIA??? 
                //id de salas de btcas dsde BD == la sala de esa funcion con id capturado

                /***ME INTERESAN SOLO LAS BUTACAS DE ESTA SALA ID***/ 
                if(butaks.sala.sala_id===this.funcionSala_IdButaca){ //si ta  llena, elejir otra
               // console.log(" IF 1 butaks.sala.sala_id -> "+ butaks.sala.sala_id +" this.funcionSala_IdButaca FIN IF 1-> "+ this.funcionSala_IdButaca );  console.log("butaks.fila -> "+ butaks.fila); console.log("butaks.numero  -> "+ butaks.numero );console.log("butaks.butaca_id -> "+ butaks.butaca_id);

                    if((butaks.fila===this.butacaFila) && (butaks.numero===this.butacaNumero)){
                      //console.log("IF 2 butaks.fila -> "+ butaks.fila);  console.log("this.butacaFila  -> "+ this.butacaFila );  console.log("butaks.numero -> "+ butaks.numero);   console.log("this.butacaNumero FIN IF 2 -> "+ this.butacaNumero);  
                      this.msgPorButaca="Butaca LLENA  y NO dsponible!!!!!!!";
                      saltarforEach = false;
                  
                    }else{//No ta  llena, guardarla
                      this.nuevaButaca = new Butaca();//this.nuevaButaca.butaca_id=8; // SoloxEje
             
                      this.nuevaButaca.fila=this.butacaFila;
                      this.nuevaButaca.numero=this.butacaNumero;
                      this.nuevaButaca.descripcion="SIN NOVEDADES";
                      this.nuevaButaca.update =new Date();
                      this.nuevaButaca.created = new Date();
                     this.nuevaButaca.sala=this.laFuncin.sala;
                     // console.log(this.funcionSala_IdButaca);
                     /* console.log("ELSE  butaks.sala.sala_id -> "+ butaks.sala.sala_id);  console.log("this.funcionSala_IdButaca -> "+ this.funcionSala_IdButaca);console.log("butaks.fila -> "+ butaks.fila);console.log("this.butacaFila  -> "+ this.butacaFila );  console.log("butaks.numero -> "+ butaks.numero);      console.log("this.butacaNumero FIN ELSE -> "+ this.butacaNumero);*/
                      this.msgPorButaca="Butaca VACIA  y  Dispoble !!!!!!!"; //lamar a servicio para guardar la nueva butaca! 
                    //  console.log(this.nuevaButaca);
                    }
            }
             
          }
        });
        
        console.log("mensaje: "+ this.msgPorButaca);
        return this.msgPorButaca;

  }


  
  
  getEstaFuncion(idLaFuncion){ 
    console.log("wwwwwwwwwwwwwwwwwwwww"+ this.idFuncionSeleccinada);
    console.log("RRRRRR"+ idLaFuncion);

    this.uuid = UUID.UUID();
    
    //console.log("aca uuid --> "+ this.uuid);
    //    console.log(idLaFuncion);
    // Recibo la sala funcion y capturo su id, le extraigo los objetos pelicula y sala 
       this.funcionService.getFuncionPorId(idLaFuncion).subscribe(laFuncionRequest=>{
      this.laFuncin=laFuncionRequest;

      
      this.laPelicula=new Pelicula();
      this.laPelicula=laFuncionRequest.pelicula;
      
      this.laSala=new Sala();
      this.laSala=laFuncionRequest.sala; 
       // let entradaTemp= new Entrada();
      for( let i = 0;i< this.entradaNuevas.length; i++){
        if(this.entradaNuevas[i].descripcion===this.laSala.descripcion){
          this.entradaTemp=this.entradaNuevas[i];
          console.log("kkk" + this.entradaNuevas[i].valor);
          this.precioEntrada=this.entradaNuevas[i].valor;
        }
      } 
      //Obtengo el id de sala de esta funcion para luego buscar las butacas de esta sala en esta funcion
      this.funcionSala_IdButaca=laFuncionRequest.sala.sala_id;
      //console.log( "idSala -> "+this.funcionSala_IdButaca);
      return this.laFuncin;
    });
    
  }
  

  getClienteUsurio(){ // SOLO OBTENGO EL USUARIO LOGEADO EN ESE MOMENTO

    let token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)); // traer token
    const decoderTocken =decode(token.access_token);
    let userClienteId=new Userclientedto();
    userClienteId.userLoing=decoderTocken.user_name;
    userClienteId.rolLoin=decoderTocken.authorities[0];

    let varI:any;
    let client= new Cliente();
   
    this.userclientedtoService.buscarClienteUserId(userClienteId).subscribe( userClienteIdUC => {
  
      varI=userClienteIdUC;
      this.clienteUserOn.cliente_id=varI;

      console.log("ddddddddddd"+ this.clienteUserOn.cliente_id);
    
    });
  }
  pagarEntradaTarjetaPago(){

    //console.log("ffff"+ this.tipoTarjeta);
    this.pagoclientedto=new Pagoclientedto();

   // this.pagoclientedto.clienteId=this.clienteUserOn.cliente_id;
    this.pagoclientedto.tarjeta_id=this.idTarjetaPago;
    this.pagoclientedto.valorEntrada=this.precioEntrada; // tambien calcula saldo nuevo
    this.pagoclientedto.uuid= this.uuid;
    console.log(this.pagoclientedto);
    this.pagoclientedtoService.registrarPagoTarjetaCliente(this.pagoclientedto).subscribe(intRetorno=>{
      console.log(intRetorno);
    });
  }

  
  consultardatosTarjetaPago(){
    this.disponibilidadtarjetapagodto=new Disponibilidadtarjetapagodto();

    this.disponibilidadtarjetapagodto.clienteId=this.clienteUserOn.cliente_id;
    this.disponibilidadtarjetapagodto.tarjetaNumero=this.tarjetaNumero;
    this.disponibilidadtarjetapagodto.tipo=this.tipoTarjeta;
    this.disponibilidadtarjetapagodto.valorEntrada=this.precioEntrada;
    //this.pagoclientedto.uuid= this.uuid;  
    //console.log("pagCli VAAA "+this.pagoclientedto.uuid);  console.log("pagCli VAAA "+this.pagoclientedto.tarjetaNumero);    console.log("pagCli VAAA "+this.pagoclientedto.clienteId);    console.log("pagCli VAAA "+this.pagoclientedto.valorEntrada);    console.log("pagCli VAAA "+this.pagoclientedto.tipo);
  
    /*{
	"clienteId":2,
	"tarjetaNumero":"1111111111111111",
	"tipo":"DEBITO",	
	"valorEntrada":23, 
  }*/
     // this.tarjetaSaldodto= new Tarjetasaldodto();
      //console.log(this.pagoclientedto);   // tarjetaSaldodtoreturn => idTarjeta:number;saldo:number
    this.pagoclientedtoService.getEstadoPagoTarjetaCliente(this.disponibilidadtarjetapagodto).subscribe((tarjetaSaldodtoreturn:Tarjetasaldodto)=>{   // console.log(tarjetaSaldodtoreturn);
     this.tarjetaSaldodto=tarjetaSaldodtoreturn;   console.log( this.tarjetaSaldodto);

     // console.log(this.tarjetaSaldodto.saldo);
      if(this.tarjetaSaldodto.idTarjeta < 0){
             this.tarjetaReturn= "INGRSE CORRECTAMENTE LOS DATOS DE A TARJETA";  
      }else if ((this.tarjetaSaldodto.idTarjeta >= 0) && (this.tarjetaSaldodto.saldo >= 0)){
          this.idTarjetaPago=this.tarjetaSaldodto.idTarjeta;
          this.flagPago=true;
          this.tarjetaReturn="LA OPERACION PUEDE REALIZARSE CORRECTAMENTE";
        }else{
        this.tarjetaReturn="TARJETA SIN DINERO SUFICIENTE";
      }
    });
    //enviar hacia  backEnd Cliente, valorEntrada y Tarjeta en un DTO
    return this.tarjetaReturn;
  }
  
  
  aceptar() {
    let ocupacionNueva=new Ocupacion();
   
    let varId ;

    /*let token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)); // traer token
     const decoderTocken =decode(token.access_token);
     let userClienteId=new Userclientedto();
     userClienteId.userLoing=decoderTocken.user_name;
     userClienteId.rolLoin=decoderTocken.authorities[0];*/

    let funcionNew = new Funcion();
    funcionNew.funcion_id = this.laFuncin.funcion_id;
    let butk:any;
    let butakka= new Butaca();
    this.butacaService.registrar(this.nuevaButaca).subscribe(butId=>{
      butk = butId;
      butakka.butaca_id=butk;
     console.log("idBtaca"+butId);
    // console.log(funcionNew.funcion_id);
  let clientt= new Cliente();
  //clientt.cliente_id=this.clienteUserOn;
 //////   let varI:any;
       //////  this.userclientedtoService.buscarClienteUserId(userClienteId).subscribe( userCliente => {

        //////   varI=userCliente;
        //////   clientt.cliente_id=varI;
          
           
           //ticket.ticket_id=100;
        //  
          this.ticketNew.cliente=this.clienteUserOn;
           this.ticketNew.fecha_transaccion=new Date();
           this.ticketNew.butacas=1;
           this.ticketNew.importe=this.entradaTemp.valor;
           this.ticketNew.pago_uuid=this.uuid;
           this.ticketNew.created=new Date();
           this.ticketNew.update=new Date();
           let tk:any;
           let tkk= new Ticket();
          console.log(this.ticketNew);
        this.ticketService.registrar(this.ticketNew).subscribe(nuevoTicket=>{
            tk=nuevoTicket;
            tkk.ticket_id=tk;
            
            //console.log("ticketID"+tk);console.log("ticketID"+nuevoTicket); console.log("TIPO DE SALA " + this.laSala.descripcion);
            /*this.entradaNueva.descripcion=this.laSala.descripcion;
            this.entradaNueva.valor;
            this.entradaNueva.created=new Date();
            this.entradaNueva.update=new Date();
            let ent:any;
            let entradd= new Entrada();
           console.log(this.ticketNew);*/
            //this.entradaService.registrar(this.entradaNueva).subscribe(nuevaEntrada=>{
              this.entradaService.getlistarEntrada().subscribe(nuevaEntrada=>{ //console.log(nuevaEntrada.length);
               /* for( let i = 0;i< nuevaEntrada.length; i++){
                  if(nuevaEntrada[i].descripcion===this.laSala.descripcion){
                    this.ocupacionNueva.entrada=nuevaEntrada[i];
                  }
                }*/                
              /*ent=nuevaEntrada;
              entradd.entrada_id=ent;*/
             // entradd=nuevaEntrada;
              this.ocupacionNueva.funcion=funcionNew;
              this.ocupacionNueva.butaca=butakka;
              this.ocupacionNueva.ticket=tkk;
             this.ocupacionNueva.entrada=this.entradaTemp;
              this.ocupacionNueva.valor=this.entradaTemp.valor;
              this.ocupacionNueva.created=new Date();
              this.ocupacionNueva.update=new Date();

              this.ocupacionService.registrar(this.ocupacionNueva).subscribe(nuevaOcup=>{
                this.pagarEntradaTarjetaPago();
                console.log(nuevaOcup);  
              });

              console.log(this.ocupacionNueva);
            });

           
            

          }); 


       //   console.log(userCliente);
         
          //this.ocupacionNueva.ticket=

        //return userCliente; 
        //////  });

   });
   //this.clienteService.
  // console.log("bb"+this.buscarClinte());
   this.limpiarControles();
   // location.reload();
  }

  buscarClinte(){
  
    let token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)); // traer token
   // let access_token = token.access_token;

    const decoderTocken =decode(token.access_token);
  
    let userClienteId=new Userclientedto();
    userClienteId.userLoing=decoderTocken.user_name;
    userClienteId.rolLoin=decoderTocken.authorities[0];

    

    this.userclientedtoService.buscarClienteUserId(userClienteId).subscribe( userCliente => {
       console.log(userCliente);

      //return userCliente; 
    });
  }  


  limpiarControles() {
    this.butacaFila ='';
    this.idFuncionSeleccinada =-1;
    this.butacaNumero=-1;
    this.laFuncin=null;
  }


  registrarButaca(but:Butaca){
    var varId ;
    this.butacaService.registrar(but).subscribe(butId=>{
     varId = butId;
    // console.log(varId);
    });
    //console.log(but);
    console.log(varId);
    return varId;
    //return "id";
  }
  listarFunciones(){
    this.funcionService.getlistarFuncion().subscribe(data=>{
      //console.log(data);
      this.funciones=data;
    });

  }

  listarButacas(){
    this.butacaService.getlistarButaca().subscribe(data=>{
    //  console.log(data);
      this.butacas=data;
    });
}
  listarSalas(){
    this.salaService.getlistarSala().subscribe(data=>{
   //   console.log(data);
      this.salas=data;
    });
  }
  listarEntradas(){
    this.entradaService.getlistarEntrada().subscribe(nuevaEntradas=>{ //console.log(nuevaEntrada.length);
    this.entradaNuevas=nuevaEntradas;
    });
  }
  /* agregar(){

      if (this.butacaFila != null && this.butacaNumero != null) {
        let but = new Butaca();
        but.fila = this.butacaFila;
        but.numero = this.butacaNumero;
        console.log(this.butacaNumero);
        console.log(this.butacaFila);
        this.butacasNew.push(but);
        console.log(this.butacasNew);
        this.butacaFila = null;
        this.butacaNumero = null;
      } //else {
        //this.mensaje = `Debe agregar una Butaca `;
        //this.snackBar.open(this.mensaje, "Aviso", { duration: 2000 });
      //}   
  }*/

  removerSala(index: number) {
    this.butacasNew.splice(index, 1);
  }

  removeFuncion(index: number) {
    this.butacasNew.splice(index, 1);
  }  
  /*
  agregarButaca(){
    if (this.idButacaSeleccinada > 0) {
      let cont = 0;
      for (let i = 0; i < this.butacasNew.length; i++) {
        let butaca = this.butacasNew[i];
        if (butaca.butaca_id === this.idButacaSeleccinada) {
          cont++;
          break;
        }
      }
      if (cont > 0) {
        this.mensaje = `El examen se encuentra en la lista`;
        this.snackBar.open(this.mensaje, "Aviso", { duration: 2000 });
      } else {
        let butacak = new Butaca();
        butacak.butaca_id = this.idButacaSeleccinada;
        this.butacaService.getButacaPorId(this.idButacaSeleccinada).subscribe(data => {
          butacak.descripcion = data.descripcion;
          this.butacasNew.push(butacak);
        });
      }
    } else {
      this.mensaje = `Debe agregar un examen`;
      this.snackBar.open(this.mensaje, "Aviso", { duration: 2000 });
    }
  }*/

  
}

/*BD:
	"/comprar"-> <List<CompraDTO>> listarPreCompraTodas()
	private Butaca butacas;
  private Funcion funcion;
  */
  
