import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatTableDataSource, MatPaginator, MatSnackBar } from '@angular/material';
import { Entrada } from '../../_model/entrada';
import { EntradaService } from '../../_service/entrada.service';

@Component({
  selector: 'app-entrada',
  templateUrl: './entrada.component.html',
  styleUrls: ['./entrada.component.css']
})
export class EntradaComponent implements OnInit {
//VARIABLES LOCALES
  //listaClientesC: Cliente[]=[];
  displayedColumns = ['entrada_id', 'descripcion', 'valor', 'created','update','acciones'];
  dataSource:MatTableDataSource<Entrada>;
  mensaje:string;
 // cantidad:number;
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  /*@ViewChild(MatPaginator) paginatorr: MatPaginator;
  // @ViewChild(MatSort) sortt: MatSort;*/
   
  //INYECCONES
  constructor(private entradaService:EntradaService,public route:ActivatedRoute, public snackBar:MatSnackBar) {

   }

  ngOnInit() {  //INICIALIZANDO VARIABLES LOCALES QUE SE REFLEJAN EN EL html
    
    //paciente service recibe un observable
    this.entradaService.entradaCambio.subscribe(listaReactivaCambiada => {//subscribe contiene contenido de observable
     this.dataSource = new MatTableDataSource(listaReactivaCambiada);
     this.dataSource.paginator= this.paginator;
      this.dataSource.sort= this.sort;
    });

    this.entradaService.mensaje.subscribe(data => {
      this.snackBar.open(data,null,{
      duration:300,
      });
   });

    this.entradaService.getlistarEntrada().subscribe(entradaObservr => {
      this.dataSource = new MatTableDataSource((entradaObservr));
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort= this.sort;  
     }); 
  }
  
  applyFilter(filterValue:string ){ // recibe texto     
    filterValue = filterValue.trim(); // limpia espacios en blanco
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue; // busca texto 
  }

  eliminar(entrada: Entrada): void {
    this.entradaService.eliminar(entrada).subscribe(data => {
      if (data === 1) {
        this.entradaService.getlistarEntrada().subscribe(ent => {
          this.entradaService.entradaCambio.next(ent);
          this.entradaService.mensaje.next("Se elimino correctamente");
        });
      } else {
        this.entradaService.mensaje.next("No se pudo eliminar");
      }

    });
  }
}