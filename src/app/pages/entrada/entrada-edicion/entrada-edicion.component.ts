import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Entrada } from '../../../_model/entrada';
import { EntradaService } from '../../../_service/entrada.service';

@Component({
  selector: 'app-entrada-edicion',
  templateUrl: './entrada-edicion.component.html',
  styleUrls: ['./entrada-edicion.component.css']
})
export class EntradaEdicionComponent implements OnInit {


  id:number;
  entrada:Entrada;
  form: FormGroup;
  edicion: boolean=false; //edicion o nuevo cliente
  
   constructor(private entradaService:EntradaService, private route:ActivatedRoute, private router:Router) { 

    this.entrada= new Entrada();

    //cajas del html-formulario. Paso valores en blanco hacia el formulario 
    this.form = new FormGroup({
    'id':new FormControl(0),
    'descripcion':new FormControl(''),
    'valor':new FormControl('')
  });
}

  ngOnInit() { //capturar id_cliente 
    this.route.params.subscribe( (parametrosUrl:Params) =>{
      this.id = parametrosUrl['id']; // Solo obtengo this.id Global
      /* si hay un id (obvio que es !=null) = > edición == V sino F*/
       this.edicion = parametrosUrl['id'] != null;// v=v=v => v , sino F 
       this.initForm();
    });
  }

  private initForm() {
    if(this.edicion) { //UPDATE
      this.entradaService.getEntradaPorId(this.id).subscribe(backEndEntradaId => {
        //obtengo los valores desde el Servicio quién recibio json con data de backEnd
        let id =  backEndEntradaId.entrada_id;
        let descripcion = backEndEntradaId.descripcion;//TRAIGO
        let valor = backEndEntradaId.valor;//CLIENTE
        //PASO backEndClienteId a formuario
        let created = backEndEntradaId.created;
 
        //PASO backEndClienteId a formuario
        this.form = new FormGroup({
          'id':new FormControl(id),
          'descripcion':new FormControl(descripcion),
          'valor':new FormControl(valor),
          'created':new FormControl(created)
        });
      });
    }
  }

  //capturando valoes de cajas de texto en formulario
  operar(){ // Capturo contenido de cajas/campos  de formulario y vinculado con formControlName
    this.entrada.entrada_id = this.form.value['id'];
    this.entrada.descripcion = this.form.value['descripcion'];
    this.entrada.valor = this.form.value['valor'];

    this.entrada.update =new Date();

    if ( /*this.edicion*/ this.entrada !=null && this.entrada.entrada_id > 0) { // viene algo, un cliente existente
      //UPDATE
      this.entrada.created = this.form.value['created'];
      this.entradaService.modificar(this.entrada).subscribe(returnBackEnd => {
       
        console.log(returnBackEnd);

        if(returnBackEnd === 1){ //1 = modificación exitosa
          /***Luego de modificarl llamo nuevamente a componente y muestre cambios***/
          //preparamos para mostrar nuevamente los datos en clienteComponet 
          this.entradaService.getlistarEntrada().subscribe(listaButacasBackEnd => {
            console.log(listaButacasBackEnd);

            //La listaClientesBackEnd ahora es reactiva, y será llamada desde clienteComponent via clienteCambioReactivo.
            this.entradaService.entradaCambio.next(listaButacasBackEnd);
            this.entradaService.mensaje.next("Se modificó la entrada");
            //CON NEXT INDICO A clienteCambioReactivo QUE SE EXE DONDE SE INSTACIE
          });
        }else{//0 = modificación NO exitosa
          this.entradaService.mensaje.next("No se modificó la entrada");
        }
      });
    }else{
      //INSERT (inicio consumo de servicio REST)
      this.entrada.created = new Date();
      this.entradaService.registrar(this.entrada).subscribe(returnBanckEnd=>{
        
          if(returnBanckEnd > 0){// 1 = exito
            this.entradaService.getlistarEntrada().subscribe(listaEntradasBackEnd=>{
              console.log(listaEntradasBackEnd);

              this.entradaService.entradaCambio.next(listaEntradasBackEnd);
              this.entradaService.mensaje.next("Se registró el butaca");
            }); 
            
          }else{ // 0 = error!
            this.entradaService.mensaje.next("No Se registró el butaca");
          }
      });
    }

    this.router.navigate(['entrada']);
  }
}
