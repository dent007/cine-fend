import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatButtonModule, 
  MatIconModule, 
  MatMenuModule,
  MatSidenavModule,
  MatDividerModule,
  MatToolbarModule,
  MatFormFieldModule, 
  MatTableModule, 
  MatPaginatorModule, 
  MatSortModule,
  MatInputModule,
  MatCardModule,
  MatSnackBarModule,
  MatSelectModule,
  MatPaginatorIntl,
  MatDialogModule,
  MatDatepickerModule,
  MatListModule,
  MatExpansionModule,
  MatNativeDateModule,
  MatAutocompleteModule} from '@angular/material';
import { MatPaginatorImpl } from '../_shared/mat-paginator';


@NgModule({
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatSidenavModule,
    MatDividerModule,
    MatToolbarModule,
    MatFormFieldModule, 
    MatTableModule, 
    MatPaginatorModule, 
    MatSortModule,
    MatInputModule,
    MatCardModule,
    MatSnackBarModule,
    MatDialogModule,
    MatSelectModule,
    MatDatepickerModule,
    MatListModule,
    MatExpansionModule,
    MatNativeDateModule,
    MatAutocompleteModule
  ],
  exports:[
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatSidenavModule,
    MatDividerModule,
    MatToolbarModule,
    MatFormFieldModule, 
    MatTableModule, 
    MatPaginatorModule, 
    MatSortModule,
    MatInputModule,
    MatCardModule,
    MatSnackBarModule,
    MatDialogModule,
    MatSelectModule,
    MatDatepickerModule,
    MatListModule,
    MatExpansionModule,
    MatNativeDateModule,
    MatAutocompleteModule
  ],
  providers: [{provide:MatPaginatorIntl,useClass:MatPaginatorImpl}],
  declarations: []
})
export class MaterialModule { }