import { Cliente } from "../_model/cliente";

export class Tarjeta{
    tarjera_id:number;
    numero:number;
    cliente:Cliente;
    saldo:number;
    created:Date;
    update:Date;
}