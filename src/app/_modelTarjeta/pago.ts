import { Tarjeta } from "./tarjeta";

export class Pago{
    pago_id:number;
    tarjeta:Tarjeta;
    importe:number;
    pagouuid:string;
    created:Date;
    update:Date;
}