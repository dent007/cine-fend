import { Not403Component } from './pages/not403/not403.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClienteComponent } from './pages/cliente/cliente.component';
import { ClienteEdicionComponent } from './pages/cliente/cliente-edicion/cliente-edicion.component';
import { LoginComponent } from './login/login.component';
import { GuardService } from './_service/guard.service';
import { ButacaComponent } from './pages/butaca/butaca.component';
import { ButacaEdicionComponent } from './pages/butaca/butaca-edicion/butaca-edicion.component';
import { TicketComponent } from './pages/ticket/ticket.component';
import { TicketEdicionComponent } from './pages/ticket/ticket-edicion/ticket-edicion.component';
import { CalificacionComponent } from './pages/calificacion/calificacion.component';
import { CalificacionEdicionComponent } from './pages/calificacion/calificacion-edicion/calificacion-edicion.component';
import { EntradaComponent } from './pages/entrada/entrada.component';
import { EntradaEdicionComponent } from './pages/entrada/entrada-edicion/entrada-edicion.component';
import { SalaComponent } from './pages/sala/sala.component';
import { SalaEdicionComponent } from './pages/sala/sala-edicion/sala-edicion.component';
import { PeliculaComponent } from './pages/pelicula/pelicula.component';
import { PeliculaEdicionComponent } from './pages/pelicula/pelicula-edicion/pelicula-edicion.component';
import { FuncionComponent } from './pages/funcion/funcion.component';
import { FuncionEdicionComponent } from './pages/funcion/funcion-edicion/funcion-edicion.component';
import { OcupacionComponent } from './pages/ocupacion/ocupacion.component';
import { OcupacionEdicionComponent } from './pages/ocupacion/ocupacion-edicion/ocupacion-edicion.component';
import { RegistroComponent } from './resgistro/resgistro.component';
import { ComprardtoComponent } from './pages/comprardto/comprardto.component';

const routes: Routes = [
  
{ path: 'cliente', component: ClienteComponent, children:[
  { path:'nuevo',component: ClienteEdicionComponent},
  { path:'edicion/:id',component: ClienteEdicionComponent}// :id valor dinámmico enviado por url   
  ], canActivate: [GuardService] // protegerá a cliente e hijos*/
},

{ path: 'butaca', component: ButacaComponent, children:[
  { path:'nuevo',component: ButacaEdicionComponent},
  { path:'edicion/:id',component: ButacaEdicionComponent}// :id valor dinámmico enviado por url   
], canActivate: [GuardService] // protegerá a cliente e hijos*/
},

{ path: 'ticket', component: TicketComponent, children:[
  { path:'nuevo',component: TicketEdicionComponent},
  { path:'edicion/:id',component: TicketEdicionComponent}// :id valor dinámmico enviado por url   
], canActivate: [GuardService] // protegerá a cliente e hijos*/
},

{ path: 'calificacion', component: CalificacionComponent, children:[
  { path:'nuevo',component: CalificacionEdicionComponent},
  { path:'edicion/:id',component: CalificacionEdicionComponent}// :id valor dinámmico enviado por url   
], canActivate: [GuardService] // protegerá a cliente e hijos*/
},

{ path: 'entrada', component: EntradaComponent, children:[
  { path:'nuevo',component: EntradaEdicionComponent},
  { path:'edicion/:id',component: EntradaEdicionComponent}// :id valor dinámmico enviado por url   
], canActivate: [GuardService] // protegerá a cliente e hijos*/
},

{ path: 'sala', component: SalaComponent, children:[
  { path:'nuevo',component: SalaEdicionComponent},
  { path:'edicion/:id',component: SalaEdicionComponent}// :id valor dinámmico enviado por url   
], canActivate: [GuardService] // protegerá a cliente e hijos*/
},

{ path: 'pelicula', component: PeliculaComponent, children:[
  { path:'nuevo',component: PeliculaEdicionComponent},
  { path:'edicion/:id',component: PeliculaEdicionComponent}// :id valor dinámmico enviado por url   
], canActivate: [GuardService] // protegerá a cliente e hijos*/
},

{ path: 'funcion', component: FuncionComponent, children:[
  { path:'nuevo',component:FuncionEdicionComponent},
  { path:'edicion/:id',component: FuncionEdicionComponent}// :id valor dinámmico enviado por url   
], canActivate: [GuardService] // protegerá a cliente e hijos*/
},

{ path: 'ocupacion', component: OcupacionComponent, children:[
  { path:'nuevo',component:OcupacionEdicionComponent},
  { path:'edicion/:id',component: OcupacionEdicionComponent}// :id valor dinámmico enviado por url   
], canActivate: [GuardService] // protegerá a cliente e hijos*/
},

{ path: 'registro', component: RegistroComponent },

{ path: 'not-403', component: Not403Component },

{ path: 'comprardto', component: ComprardtoComponent,
  canActivate: [GuardService] 
},

{ path: 'login', component: LoginComponent },
  { path: '', redirectTo: 'login', pathMatch: 'full' } // ante localhost:4200 , andate al login
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }