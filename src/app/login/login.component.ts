import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../_service/login.service';
import { TOKEN_NAME } from '../_shared/var.constant';
import * as decode from 'jwt-decode'; //decodifica tcken pa extraer nombre, clave , rol, tpo de vida de sssion : desde access_token pero del JSON
//npm install jwt-decode --save

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  usuario: string;
  contrasena: string;
  //Inyecto
  constructor(private loginService: LoginService, private router: Router) { }
   


  ngOnInit() {
    
 //this.cerrarSesion();
  }
  
  iniciarSesion() {//login se comunica con servicio "oauth" <--> http://localhost:8080/oauth/token y trae/mete TODO ese JSON en data
     this.loginService.login(this.usuario, this.contrasena).subscribe( jsonResposCompleto => {      
      //
      if (jsonResposCompleto) {
        //JSON.stringify() convierte un objeto o valor de JavaScript "data"en una cadena de texto JSON
        let token = JSON.stringify(jsonResposCompleto);

//sessionStorage almacena/guarda datos en pag mientras pestaña este open. sessionStorage.setItem('Nombre', 'Miguel Antonio')
          //https://alligator.io/js/introduction-localstorage-sessionstorage/
          /***LOS OTROS COMPONENTES CONSUMIRÄN ESTE sessionStorage PARA HACER SUS PETICIONES extrayendo el access-tocken****/
        sessionStorage.setItem(TOKEN_NAME, token);//setItem añade "clave:valor" al almacen/Storage o los actualiza
        //pasado todo, navego a cliente
        this.router.navigate(['comprardto']);
      }
    });


    /////otro
    this.usuario=null;// o '';
    this.contrasena=null;// o  '';
  }
  cerrarSesion() {    
    sessionStorage.clear();
    this.router.navigate(['login']);
  }
}
//revisado