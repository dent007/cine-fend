import { Cliente } from './../_model/cliente';
import { Injectable } from "@angular/core";
import { HOST, TOKEN_NAME, CINE_W } from "../_shared/var.constant";
import { Router } from "@angular/router";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable()
export class RegistroService {

  // El tocken necesita un usuario y clave
    private url: string = `${HOST}/${CINE_W}/cliente`;

    constructor(private http: HttpClient) {
      
    }    
        nuevoRegistro(cliente:Cliente){
      //      let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
            return this.http.post(`${this.url}/registrar`,cliente/*,{
              headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json') 
            }*/);  
          }
}