import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { LoginService } from './login.service';
import { TOKEN_NAME } from '../_shared/var.constant';
import { tokenNotExpired } from 'angular2-jwt'; // npm install angular2-jwt --save
import * as decode from 'jwt-decode';

@Injectable() // el tocken ya est+a activo protegiendo los servicios. Solo si obtengo un tck traigo el rest
//this componente gestiona si tiempo de vida de tocken expiró de ese usuario. pasad el tpo app sabe q tpo de vida expiró
//redireciona a otro sitio "loging" si no se "volvió a logear" o no me permita navergar si no regeneró tocken 
export class GuardService implements CanActivate{ // si tpo de via de tckn expiro, Guard redirecciona a otra vista => user gestiona otro 

  constructor(private loginService: LoginService, private router: Router) { }
  // return V si tocken está activo  o F si no lo está 
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {//retorna V(toekn activo) o F (lo contrario)
    /*Si token está activo= true=> pasa a la página ; sino no pasa , se redirecciona a login  */
    let token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)); // traer token

      if(token !=null){ //this vacio? EXISTE AUN?
    
        let access_token = token.access_token;
      
        let rpta = this.loginService.estaLogeado(); // estaLogeado retorna v/f si está no logeado

        
    
        if(!rpta){ // es F => limpia sess-- y envoa a login y retorna F
          sessionStorage.clear();
          this.router.navigate(['login']);
          return false;
        }else{  // SI EXISTE, PERO EL TIEMPO DE VIDA YA EXPIRÓ
          if(tokenNotExpired(TOKEN_NAME,access_token)){ // no expiró, ta vivo!!
            // npm install angular2-jwt --save -> tokenNotExpired

            const decoderTocken =decode(token.access_token);
            // //npm install jwt-decode --save -> e import * as decode from 'jwt-decode';
          //  console.log(decoderTocken);
            let rol = decoderTocken.authorities[0];
            let us = decoderTocken.user_name;
            //console.log(us);
            let  url= state.url;

            switch(rol){
              case 'ROLE_ADMIN': {
                if (url==='/cliente' || url==='/ticket' || url==='/entrada' || url==='/sala' || url==='/butaca' || 
                url==='/calificacion' || url==='/pelicula' || url==='/funcion' || url==='/ocupacion' || url==='/comprardto'){
                  return true;
                }else{
                  this.router.navigate(['not-403']);
                  return false;
                }
              }

              case 'ROLE_USER': {
                if(url==='/comprardto'){
                  return true;
                }else{
                  this.router.navigate(['not-403']);
                  return false;
                }
              }
              
              default:{
                  this.router.navigate(['not-403']);
                  return false;
                
              }

            }
          //  return true;
          }else{ // Expiró, limpia sess-- y envoa a login y retorna F
            sessionStorage.clear();
            this.router.navigate(['login']);
            return false;
          }
        }

      }else{ // si no existe limpio sess.. y redirijo a login
        sessionStorage.clear();
        this.router.navigate(['login']);
        return false; // retorno f pq no pase a pagina.
      }

  }

}
