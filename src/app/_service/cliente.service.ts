import { TOKEN_NAME, CINE_W } from './../_shared/var.constant';
 import { Cliente } from './../_model/cliente';
 import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
//import { Subject } from 'rxjs/Subject';
import { HOST } from '../_shared/var.constant';
import { Subject } from 'rxjs/Subject';

@Injectable() 
export class ClienteService { // registrar en app.module ->providers

  url:string =  `${HOST}/${CINE_W}/cliente`; 

  //Programación reactiva: instalcia con subdato "array de clientes dentro"
  clienteCambioReactivo = new Subject<Cliente[]>(); // Subject es de RXjs
  
  mensajeReactivo = new Subject<string>();
 
  constructor(private http: HttpClient) { //HttpClient -> soporta respuesta JSON. Registrarlo en app.module
  }
  
  // Quien llamé a esta función, recibe un observable que dentro trae "objetoObservalble(cliente[])"
  getlistarCliente(){ // el get retornará una lista de pacientes (automaticamente Angular resibe el response y retorna una lista de clientes)
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;

    return this.http.get<Cliente[]>(`${this.url}/listar`,{
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json') 
    }) ;
  }

  getlistarClienteByPages(p:number, s:number){ // el get retornará una lista de pacientes (automaticamente Angular resibe el response y retorna una lista de clientes)
    
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
       
      //http://localhost:8080/cliente/listarPageable?page=0&size=2
      return this.http.get<Cliente[]>(`${this.url}/listarPageable?page=${p}&size=${s}`,{
        headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json') 
      }) ;      
  }

  registrar(cliente:Cliente){

    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.post(`${this.url}/registrar`,cliente,{
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json') 
    });  
  }
  modificar(cliente:Cliente){
    
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.put(`${this.url}/actualizar`,cliente,{
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json') 
    });  
   }
   eliminar(cliente:Cliente){

    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.delete(`${this.url}/eliminar/${cliente.cliente_id}`,{
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json') 
    });  
   }
   getClientePorId(id:number){

    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.get<Cliente>(`${this.url}/listar/${id}`,{
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json') 
    });
   }
} 
