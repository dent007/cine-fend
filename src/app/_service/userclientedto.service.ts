import { Userclientedto } from './../_model/userclientedto';
import { HOST, TOKEN_NAME, COMPRA_W } from './../_shared/var.constant';
import { Subject } from 'rxjs/Subject';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Butaca } from '../_model/butaca';

@Injectable()
export class UserclientedtoService {

  private url: string = `${HOST}/${COMPRA_W}/compra`;
  constructor(private http: HttpClient) { }

  buscarClienteUserId(userclientedto: Userclientedto) {
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.post(`${this.url}/cliente`, userclientedto, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }
}