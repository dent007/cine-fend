import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { HOST, TOKEN_AUTH_USERNAME, TOKEN_AUTH_PASSWORD, TOKEN_NAME, OUAT_W } from './../_shared/var.constant';
import * as decode from 'jwt-decode'; //decodifica tcken pa extraer nombre, clave , rol, tpo de vida de sssion : desde access_token pero del JSON
//npm install jwt-decode --save


@Injectable()
export class LoginService {

  //private url: string = `${HOST}/oauth/token`;
  private url: string = `${HOST}/${OUAT_W}/oauth/token`;

  constructor(private http: HttpClient, private router: Router) {
  }

  login(usuario: string, contrasena: string) {// user y contra viene caja de text desde formulario login html
    const body = `grant_type=password&username=${encodeURIComponent(usuario)}&password=${encodeURIComponent(contrasena)}`;

    return this.http.post(this.url, body, { // retorna un observable con todo el JSON: access-token, token_type, expires_in, scope, etc
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8').set('Authorization', 'Basic ' + btoa(TOKEN_AUTH_USERNAME + ':' + TOKEN_AUTH_PASSWORD))

    });
    //return true;
  }

  estaLogeado() { // inspecciona si hay o no token para retornar v/f
    let token = sessionStorage.getItem(TOKEN_NAME);
    return token != null; // retorna V/F si existe no el tocken
  }

  cerrarSesion() {
    sessionStorage.clear();
    this.router.navigate(['login']);
  }

    
    isAdmin(){ // retorna V o F para el *ngIf
        let tk=sessionStorage.getItem(TOKEN_NAME);
        if(tk !=null){
            const decodedToken = decode(tk);
            /*Consulto si exste tocken, si existe retorna isAdmin = V , si no F */
            let isAdmin=decodedToken.authorities.some(e1=>
              e1 ==='ROLE_ADMIN'
            );
            return isAdmin;  //v/f pa *ngIf
        }
    }
}
///Revisado