import { Injectable } from "@angular/core";
import { HOST, TOKEN_NAME, CINE_W } from "../_shared/var.constant";
import { Subject } from "rxjs/Subject";
import { Ocupacion } from "../_model/ocupacion";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable()
export class ComprardtoService {
    private url: string = `${HOST}/${CINE_W}/ocupacion`;
    
    ocupacionCambio = new Subject<Ocupacion[]>();
    mensaje = new Subject<string>();

    constructor(private http: HttpClient) { }

getListarPreCompra(){
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.get<Ocupacion[]>(`${this.url}/comprar`, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }


registrarCompraPelicula(ocupacion: Ocupacion) {
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.post(`${this.url}/registrar`, ocupacion, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }
}
/*BD:
http://localhost:8080/ocupacion/comprar
	"/comprar"-> <List<CompraDTO>> listarPreCompraTodas()
	private Butaca butacas;
	private Funcion funcion;*/
